// ------------------------------------------------------
// generated by builder.sce: Please do not edit this file
// ------------------------------------------------------

snns_path = get_absolute_file_path('loader.sce');

// ulink previous function with same name
[bOK,ilib] = c_link('snns');if (bOK) then ulink(ilib),end
link(snns_path+'libsnns.so',['snns'],'c');
// remove temp. variables on stack
clear snns_path;
clear get_file_path;
// ------------------------------------------------------
