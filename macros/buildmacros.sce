// ====================================================================
// Copyright 2009
// Yann COLLETTE
// Emmanuel GOOSSAERT
// This file is released into the public domain
// ====================================================================

tbx_build_macros(TOOLBOX_NAME, get_absolute_file_path('buildmacros.sce'));

clear tbx_build_macros;
