#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


krui_err krui_testAllPatterns(float*, int, float**, int*);
krui_err krui_testSinglePattern(int, float*, int, float**, int*);
krui_err krui_setPatternNo(int);
krui_err krui_getPatternNo();
krui_err krui_deletePattern();
krui_err krui_modifyPattern();
krui_err krui_showPattern(int);
krui_err krui_allocNewPatternSet(int*);
krui_err krui_newPattern();
int krui_getNoOfPatterns();
int krui_getTotalNoOfSubPatterns();
void krui_deleteAllPatterns();
krui_err krui_shufflePatterns(bool);
krui_err krui_shuffleSubPatterns(bool);
krui_err krui_setCurrPatSet(int);
krui_err krui_deletePatSet(int);
krui_err krui_GetPatInfo(pattern_set_info*, pattern_descriptor*);
krui_err krui_DefShowSubPat(int*, int*, int*, int*);
krui_err krui_DefTrainSubPat(int*, int*, int*, int*, int*);
krui_err krui_AlignSubPat(int*, int*, int*);
krui_err krui_GetShapeOfSubPattern(int*, int*, int*, int*, int);
krui_err krui_loadNewPatterns(char*, int*);
krui_err krui_saveNewPatterns(char*, int);


int sci_ann_test_all_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_testAllPatterns(parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(3, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_test_single_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_pattern_id;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_pattern_id = 1, col_pattern_id = 1;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_pattern_id = NULL;
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int pattern_id;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_pattern_id);
    getVarAddressFromPosition(3, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_pattern_id, &row_pattern_id, &col_pattern_id);
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_pattern_id, &row_pattern_id, &col_pattern_id, &var_pattern_id);
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    pattern_id = *var_pattern_id;
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_testSinglePattern(pattern_id, parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(4, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_set_pattern_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_pattern_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_pattern_id = 1, col_pattern_id = 1;

    /*** Address parameters ***/
    int *ptr_pattern_id = NULL;

    /*** Source parameters ***/
    int sci;
    int pattern_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_pattern_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_pattern_id, &row_pattern_id, &col_pattern_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_pattern_id, &row_pattern_id, &col_pattern_id, &var_pattern_id);

    /*** Parameter copy ***/
    pattern_id = *var_pattern_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setPatternNo(pattern_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_pattern_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getPatternNo();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_delete_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deletePattern();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_modify_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_modifyPattern();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_show_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_mode;

    /*** Dimension parameters ***/
    int res_network;
    int row_mode = 1, col_mode = 1;

    /*** Address parameters ***/
    int *ptr_mode = NULL;

    /*** Source parameters ***/
    int sci;
    int mode;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_mode);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_mode, &row_mode, &col_mode);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_mode, &row_mode, &col_mode, &var_mode);

    /*** Parameter copy ***/
    mode = *var_mode;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_showPattern(mode);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_alloc_new_pattern_set(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_set_id_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_set_id = 1, col_set_id = 1;
    int row_set_id_ret = 1, col_set_id_ret = 1;

    /*** Address parameters ***/
    int *ptr_set_id_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int* set_id;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    set_id = (int*) malloc(row_set_id * col_set_id * sizeof(int));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_allocNewPatternSet(set_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_set_id_ret, col_set_id_ret, set_id);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Memory free ***/
    free(set_id);


    return 0;
}


int sci_ann_new_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_newPattern();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_no_of_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfPatterns();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_total_no_of_sub_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getTotalNoOfSubPatterns();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_delete_all_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_deleteAllPatterns();

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_shuffle_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_on_or_off;

    /*** Dimension parameters ***/
    int res_network;
    int row_on_or_off = 1, col_on_or_off = 1;

    /*** Address parameters ***/
    int *ptr_on_or_off = NULL;

    /*** Source parameters ***/
    int sci;
    int on_or_off;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_on_or_off);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_on_or_off, &row_on_or_off, &col_on_or_off);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_on_or_off, &row_on_or_off, &col_on_or_off, &var_on_or_off);

    /*** Parameter copy ***/
    on_or_off = *var_on_or_off;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_shufflePatterns(on_or_off);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_shuffle_sub_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_on_or_off;

    /*** Dimension parameters ***/
    int res_network;
    int row_on_or_off = 1, col_on_or_off = 1;

    /*** Address parameters ***/
    int *ptr_on_or_off = NULL;

    /*** Source parameters ***/
    int sci;
    int on_or_off;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_on_or_off);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_on_or_off, &row_on_or_off, &col_on_or_off);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_on_or_off, &row_on_or_off, &col_on_or_off, &var_on_or_off);

    /*** Parameter copy ***/
    on_or_off = *var_on_or_off;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_shuffleSubPatterns(on_or_off);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_set_curr_pat_set(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_number;

    /*** Dimension parameters ***/
    int res_network;
    int row_number = 1, col_number = 1;

    /*** Address parameters ***/
    int *ptr_number = NULL;

    /*** Source parameters ***/
    int sci;
    int number;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_number);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_number, &row_number, &col_number);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_number, &row_number, &col_number, &var_number);

    /*** Parameter copy ***/
    number = *var_number;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setCurrPatSet(number);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_delete_pat_set(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_number;

    /*** Dimension parameters ***/
    int res_network;
    int row_number = 1, col_number = 1;

    /*** Address parameters ***/
    int *ptr_number = NULL;

    /*** Source parameters ***/
    int sci;
    int number;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_number);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_number, &row_number, &col_number);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_number, &row_number, &col_number, &var_number);

    /*** Parameter copy ***/
    number = *var_number;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deletePatSet(number);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_pat_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    pattern_set_info *var_set_info_ret;
    pattern_descriptor *var_pat_info_ret;

    /*** Dimension parameters ***/
    int res_network;
    int res_set_info;
    int res_pat_info;
    int res_set_info_ret;
    int res_pat_info_ret;

    /*** Source parameters ***/
    int sci;
    pattern_set_info* set_info;
    pattern_descriptor* pat_info;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_GetPatInfo(set_info, pat_info);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    var_set_info_ret = set_info;
    var_pat_info_ret = pat_info;

    /*** Create return parameters ***/
    res_set_info_ret = ctos_pattern_set_info(2, var_set_info_ret);
    res_pat_info_ret = ctos_pattern_descriptor(3, var_pat_info_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;


    return 0;
}


int sci_ann_def_show_sub_pat(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_insize;
    int *var_outsize;
    int *var_inpos;
    int *var_outpos;

    /*** Dimension parameters ***/
    int res_network;
    int row_insize = 1, col_insize = 1;
    int row_outsize = 1, col_outsize = 1;
    int row_inpos = 1, col_inpos = 1;
    int row_outpos = 1, col_outpos = 1;

    /*** Address parameters ***/
    int *ptr_insize = NULL;
    int *ptr_outsize = NULL;
    int *ptr_inpos = NULL;
    int *ptr_outpos = NULL;

    /*** Source parameters ***/
    int sci;
    int* insize;
    int* outsize;
    int* inpos;
    int* outpos;

    /*** Check for number of parameters ***/
    CheckRhs(5, 5);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_insize);
    getVarAddressFromPosition(3, &ptr_outsize);
    getVarAddressFromPosition(4, &ptr_inpos);
    getVarAddressFromPosition(5, &ptr_outpos);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_insize, &row_insize, &col_insize);
    getVarDimension(ptr_outsize, &row_outsize, &col_outsize);
    getVarDimension(ptr_inpos, &row_inpos, &col_inpos);
    getVarDimension(ptr_outpos, &row_outpos, &col_outpos);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_insize, &row_insize, &col_insize, &var_insize);
    getMatrixFromDoubleTo_int(ptr_outsize, &row_outsize, &col_outsize, &var_outsize);
    getMatrixFromDoubleTo_int(ptr_inpos, &row_inpos, &col_inpos, &var_inpos);
    getMatrixFromDoubleTo_int(ptr_outpos, &row_outpos, &col_outpos, &var_outpos);

    /*** Parameter copy ***/
    insize = var_insize;
    outsize = var_outsize;
    inpos = var_inpos;
    outpos = var_outpos;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_DefShowSubPat(insize, outsize, inpos, outpos);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_def_train_sub_pat(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_insize;
    int *var_outsize;
    int *var_instep;
    int *var_outstep;
    int *var_max_n_pos;

    /*** Dimension parameters ***/
    int res_network;
    int row_insize = 1, col_insize = 1;
    int row_outsize = 1, col_outsize = 1;
    int row_instep = 1, col_instep = 1;
    int row_outstep = 1, col_outstep = 1;
    int row_max_n_pos = 1, col_max_n_pos = 1;

    /*** Address parameters ***/
    int *ptr_insize = NULL;
    int *ptr_outsize = NULL;
    int *ptr_instep = NULL;
    int *ptr_outstep = NULL;
    int *ptr_max_n_pos = NULL;

    /*** Source parameters ***/
    int sci;
    int* insize;
    int* outsize;
    int* instep;
    int* outstep;
    int* max_n_pos;

    /*** Check for number of parameters ***/
    CheckRhs(6, 6);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_insize);
    getVarAddressFromPosition(3, &ptr_outsize);
    getVarAddressFromPosition(4, &ptr_instep);
    getVarAddressFromPosition(5, &ptr_outstep);
    getVarAddressFromPosition(6, &ptr_max_n_pos);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_insize, &row_insize, &col_insize);
    getVarDimension(ptr_outsize, &row_outsize, &col_outsize);
    getVarDimension(ptr_instep, &row_instep, &col_instep);
    getVarDimension(ptr_outstep, &row_outstep, &col_outstep);
    getVarDimension(ptr_max_n_pos, &row_max_n_pos, &col_max_n_pos);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_insize, &row_insize, &col_insize, &var_insize);
    getMatrixFromDoubleTo_int(ptr_outsize, &row_outsize, &col_outsize, &var_outsize);
    getMatrixFromDoubleTo_int(ptr_instep, &row_instep, &col_instep, &var_instep);
    getMatrixFromDoubleTo_int(ptr_outstep, &row_outstep, &col_outstep, &var_outstep);
    getMatrixFromDoubleTo_int(ptr_max_n_pos, &row_max_n_pos, &col_max_n_pos, &var_max_n_pos);

    /*** Parameter copy ***/
    insize = var_insize;
    outsize = var_outsize;
    instep = var_instep;
    outstep = var_outstep;
    max_n_pos = var_max_n_pos;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_DefTrainSubPat(insize, outsize, instep, outstep, max_n_pos);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_align_sub_pat(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_inpos;
    int *var_outpos;
    int *var_no;
    int *var_inpos_ret;
    int *var_outpos_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_inpos = 1, col_inpos = 1;
    int row_outpos = 1, col_outpos = 1;
    int row_no = 1, col_no = 1;
    int row_inpos_ret = 1, col_inpos_ret = 1;
    int row_outpos_ret = 1, col_outpos_ret = 1;

    /*** Address parameters ***/
    int *ptr_inpos = NULL;
    int *ptr_outpos = NULL;
    int *ptr_no = NULL;
    int *ptr_inpos_ret = NULL;
    int *ptr_outpos_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int* inpos;
    int* outpos;
    int* no;

    /*** Check for number of parameters ***/
    CheckRhs(4, 4);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_inpos);
    getVarAddressFromPosition(3, &ptr_outpos);
    getVarAddressFromPosition(4, &ptr_no);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_inpos, &row_inpos, &col_inpos);
    getVarDimension(ptr_outpos, &row_outpos, &col_outpos);
    getVarDimension(ptr_no, &row_no, &col_no);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_inpos, &row_inpos, &col_inpos, &var_inpos);
    getMatrixFromDoubleTo_int(ptr_outpos, &row_outpos, &col_outpos, &var_outpos);
    getMatrixFromDoubleTo_int(ptr_no, &row_no, &col_no, &var_no);

    /*** Parameter copy ***/
    inpos = var_inpos;
    outpos = var_outpos;
    no = var_no;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_AlignSubPat(inpos, outpos, no);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(5, row_inpos_ret, col_inpos_ret, inpos);
    createMatrixOfDoubleFrom_int(6, row_outpos_ret, col_outpos_ret, outpos);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 5;
    LhsVar(2) = 6;


    return 0;
}


int sci_ann_get_shape_of_sub_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_n_pos;
    int *var_insize_ret;
    int *var_outsize_ret;
    int *var_inpos_ret;
    int *var_outpos_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_insize = 1, col_insize = 1;
    int row_outsize = 1, col_outsize = 1;
    int row_inpos = 1, col_inpos = 1;
    int row_outpos = 1, col_outpos = 1;
    int row_n_pos = 1, col_n_pos = 1;
    int row_insize_ret = 1, col_insize_ret = 1;
    int row_outsize_ret = 1, col_outsize_ret = 1;
    int row_inpos_ret = 1, col_inpos_ret = 1;
    int row_outpos_ret = 1, col_outpos_ret = 1;

    /*** Address parameters ***/
    int *ptr_n_pos = NULL;
    int *ptr_insize_ret = NULL;
    int *ptr_outsize_ret = NULL;
    int *ptr_inpos_ret = NULL;
    int *ptr_outpos_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int* insize;
    int* outsize;
    int* inpos;
    int* outpos;
    int n_pos;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(4, 4);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_n_pos);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_n_pos, &row_n_pos, &col_n_pos);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_n_pos, &row_n_pos, &col_n_pos, &var_n_pos);

    /*** Memory allocation ***/
    insize = (int*) malloc(row_insize * col_insize * sizeof(int));
    outsize = (int*) malloc(row_outsize * col_outsize * sizeof(int));
    inpos = (int*) malloc(row_inpos * col_inpos * sizeof(int));
    outpos = (int*) malloc(row_outpos * col_outpos * sizeof(int));

    /*** Parameter copy ***/
    n_pos = *var_n_pos;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_GetShapeOfSubPattern(insize, outsize, inpos, outpos, n_pos);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_insize_ret, col_insize_ret, insize);
    createMatrixOfDoubleFrom_int(4, row_outsize_ret, col_outsize_ret, outsize);
    createMatrixOfDoubleFrom_int(5, row_inpos_ret, col_inpos_ret, inpos);
    createMatrixOfDoubleFrom_int(6, row_outpos_ret, col_outpos_ret, outpos);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;
    LhsVar(2) = 4;
    LhsVar(3) = 5;
    LhsVar(4) = 6;

    /*** Memory free ***/
    free(insize);
    free(outsize);
    free(inpos);
    free(outpos);


    return 0;
}


int sci_ann_load_new_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_filename;
    int *var_set_id_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_filename = 1, col_filename = 1;
    int row_set_id = 1, col_set_id = 1;
    int row_set_id_ret = 1, col_set_id_ret = 1;

    /*** Address parameters ***/
    int *ptr_filename = NULL;
    int cnt_filename;
    int *lengths_filename = NULL;
    char **strings_filename = NULL;
    int *ptr_set_id_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* filename;
    int* set_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_filename);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_filename, &row_filename, &col_filename);

    /*** Prerequisites destination ***/
    lengths_filename = (int*) malloc(row_filename * col_filename * sizeof(int));
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, NULL);
    strings_filename = (char**) malloc(row_filename * col_filename * sizeof(char*));
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) strings_filename[cnt_filename] = (char*) malloc((lengths_filename[cnt_filename] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, strings_filename);

    /*** Memory allocation ***/
    set_id = (int*) malloc(row_set_id * col_set_id * sizeof(int));

    /*** Parameter copy ***/
    filename = strings_filename[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_loadNewPatterns(filename, set_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_set_id_ret, col_set_id_ret, set_id);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites destination ***/
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) free(strings_filename[cnt_filename]);
    free(strings_filename);
    free(lengths_filename);

    /*** Memory free ***/
    free(set_id);


    return 0;
}


int sci_ann_save_new_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_filename;
    int *var_set_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_filename = 1, col_filename = 1;
    int row_set_id = 1, col_set_id = 1;

    /*** Address parameters ***/
    int *ptr_filename = NULL;
    int cnt_filename;
    int *lengths_filename = NULL;
    char **strings_filename = NULL;
    int *ptr_set_id = NULL;

    /*** Source parameters ***/
    int sci;
    char* filename;
    int set_id;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_filename);
    getVarAddressFromPosition(3, &ptr_set_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_filename, &row_filename, &col_filename);
    getVarDimension(ptr_set_id, &row_set_id, &col_set_id);

    /*** Prerequisites destination ***/
    lengths_filename = (int*) malloc(row_filename * col_filename * sizeof(int));
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, NULL);
    strings_filename = (char**) malloc(row_filename * col_filename * sizeof(char*));
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) strings_filename[cnt_filename] = (char*) malloc((lengths_filename[cnt_filename] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, strings_filename);
    getMatrixFromDoubleTo_int(ptr_set_id, &row_set_id, &col_set_id, &var_set_id);

    /*** Parameter copy ***/
    filename = strings_filename[0];
    set_id = *var_set_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_saveNewPatterns(filename, set_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) free(strings_filename[cnt_filename]);
    free(strings_filename);
    free(lengths_filename);


    return 0;
}


