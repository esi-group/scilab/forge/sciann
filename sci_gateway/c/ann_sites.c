#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


bool krui_getFirstSiteTableEntry(char**, char**);
bool krui_getNextSiteTableEntry(char**, char**);
char* krui_getSiteTableFuncName(char*);
krui_err krui_createSiteTableEntry(char*, char*);
krui_err krui_changeSiteTableEntry(char*, char*, char*);
krui_err krui_deleteSiteTableEntry(char*);
bool krui_setFirstSite();
bool krui_setNextSite();
krui_err krui_setSite(char*);
FlintType krui_getSiteValue();
char* krui_getSiteName();
krui_err krui_setSiteName(char*);
char* krui_getSiteFuncName();
krui_err krui_addSite(char*);
bool krui_deleteSite();


int sci_ann_get_first_site_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    char *var_site_name_ret;
    char *var_site_func_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;
    int row_site_func = 1, col_site_func = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_site_name_ret = 1, col_site_name_ret = 1;
    int row_site_func_ret = 1, col_site_func_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_site_name_ret = NULL;
    int cnt_site_name_ret;
    int *lengths_site_name_ret = NULL;
    char **strings_site_name_ret = NULL;
    int *ptr_site_func_ret = NULL;
    int cnt_site_func_ret;
    int *lengths_site_func_ret = NULL;
    char **strings_site_func_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char** site_name;
    char** site_func;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(3, 3);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    site_name = (char**) malloc(sizeof(char*));
    site_func = (char**) malloc(sizeof(char*));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFirstSiteTableEntry(site_name, site_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_site_name_ret = 0; cnt_site_name_ret < row_site_name_ret * col_site_name_ret ; ++cnt_site_name_ret) if(site_name[cnt_site_name_ret] == NULL) { Scierror(999, "site_name is NULL"); return -1;}
    for(cnt_site_func_ret = 0; cnt_site_func_ret < row_site_func_ret * col_site_func_ret ; ++cnt_site_func_ret) if(site_func[cnt_site_func_ret] == NULL) { Scierror(999, "site_func is NULL"); return -1;}

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    strings_site_name_ret = (char**) malloc(row_site_name_ret * col_site_name_ret * sizeof(char*));
    for(cnt_site_name_ret = 0; cnt_site_name_ret < row_site_name_ret * col_site_name_ret ; ++cnt_site_name_ret) strings_site_name_ret[cnt_site_name_ret] = site_name[cnt_site_name_ret];
    createMatrixOfString(3, row_site_name_ret, col_site_name_ret, strings_site_name_ret);
    strings_site_func_ret = (char**) malloc(row_site_func_ret * col_site_func_ret * sizeof(char*));
    for(cnt_site_func_ret = 0; cnt_site_func_ret < row_site_func_ret * col_site_func_ret ; ++cnt_site_func_ret) strings_site_func_ret[cnt_site_func_ret] = site_func[cnt_site_func_ret];
    createMatrixOfString(4, row_site_func_ret, col_site_func_ret, strings_site_func_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;

    /*** Postrequisites return ***/
    free(strings_site_name_ret);
    free(strings_site_func_ret);

    /*** Memory free ***/
    free(site_name);
    free(site_func);


    return 0;
}


int sci_ann_get_next_site_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    char *var_site_name_ret;
    char *var_site_func_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;
    int row_site_func = 1, col_site_func = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_site_name_ret = 1, col_site_name_ret = 1;
    int row_site_func_ret = 1, col_site_func_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_site_name_ret = NULL;
    int cnt_site_name_ret;
    int *lengths_site_name_ret = NULL;
    char **strings_site_name_ret = NULL;
    int *ptr_site_func_ret = NULL;
    int cnt_site_func_ret;
    int *lengths_site_func_ret = NULL;
    char **strings_site_func_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char** site_name;
    char** site_func;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(3, 3);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    site_name = (char**) malloc(sizeof(char*));
    site_func = (char**) malloc(sizeof(char*));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNextSiteTableEntry(site_name, site_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_site_name_ret = 0; cnt_site_name_ret < row_site_name_ret * col_site_name_ret ; ++cnt_site_name_ret) if(site_name[cnt_site_name_ret] == NULL) { Scierror(999, "site_name is NULL"); return -1;}
    for(cnt_site_func_ret = 0; cnt_site_func_ret < row_site_func_ret * col_site_func_ret ; ++cnt_site_func_ret) if(site_func[cnt_site_func_ret] == NULL) { Scierror(999, "site_func is NULL"); return -1;}

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    strings_site_name_ret = (char**) malloc(row_site_name_ret * col_site_name_ret * sizeof(char*));
    for(cnt_site_name_ret = 0; cnt_site_name_ret < row_site_name_ret * col_site_name_ret ; ++cnt_site_name_ret) strings_site_name_ret[cnt_site_name_ret] = site_name[cnt_site_name_ret];
    createMatrixOfString(3, row_site_name_ret, col_site_name_ret, strings_site_name_ret);
    strings_site_func_ret = (char**) malloc(row_site_func_ret * col_site_func_ret * sizeof(char*));
    for(cnt_site_func_ret = 0; cnt_site_func_ret < row_site_func_ret * col_site_func_ret ; ++cnt_site_func_ret) strings_site_func_ret[cnt_site_func_ret] = site_func[cnt_site_func_ret];
    createMatrixOfString(4, row_site_func_ret, col_site_func_ret, strings_site_func_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;

    /*** Postrequisites return ***/
    free(strings_site_name_ret);
    free(strings_site_func_ret);

    /*** Memory free ***/
    free(site_name);
    free(site_func);


    return 0;
}


int sci_ann_get_site_table_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;
    char* site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getSiteTableFuncName(site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(3, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_create_site_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;
    char *var_site_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;
    int row_site_func = 1, col_site_func = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;
    int *ptr_site_func = NULL;
    int cnt_site_func;
    int *lengths_site_func = NULL;
    char **strings_site_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* site_name;
    char* site_func;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);
    getVarAddressFromPosition(3, &ptr_site_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);
    getVarDimension(ptr_site_func, &row_site_func, &col_site_func);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));
    lengths_site_func = (int*) malloc(row_site_func * col_site_func * sizeof(int));
    getMatrixOfString(ptr_site_func, &row_site_func, &col_site_func, lengths_site_func, NULL);
    strings_site_func = (char**) malloc(row_site_func * col_site_func * sizeof(char*));
    for(cnt_site_func = 0; cnt_site_func < row_site_func * col_site_func ; ++cnt_site_func) strings_site_func[cnt_site_func] = (char*) malloc((lengths_site_func[cnt_site_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);
    getMatrixOfString(ptr_site_func, &row_site_func, &col_site_func, lengths_site_func, strings_site_func);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];
    site_func = strings_site_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createSiteTableEntry(site_name, site_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);
    for(cnt_site_func = 0; cnt_site_func < row_site_func * col_site_func ; ++cnt_site_func) free(strings_site_func[cnt_site_func]);
    free(strings_site_func);
    free(lengths_site_func);


    return 0;
}


int sci_ann_change_site_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_old_site_name;
    char *var_new_site_name;
    char *var_new_site_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_old_site_name = 1, col_old_site_name = 1;
    int row_new_site_name = 1, col_new_site_name = 1;
    int row_new_site_func = 1, col_new_site_func = 1;

    /*** Address parameters ***/
    int *ptr_old_site_name = NULL;
    int cnt_old_site_name;
    int *lengths_old_site_name = NULL;
    char **strings_old_site_name = NULL;
    int *ptr_new_site_name = NULL;
    int cnt_new_site_name;
    int *lengths_new_site_name = NULL;
    char **strings_new_site_name = NULL;
    int *ptr_new_site_func = NULL;
    int cnt_new_site_func;
    int *lengths_new_site_func = NULL;
    char **strings_new_site_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* old_site_name;
    char* new_site_name;
    char* new_site_func;

    /*** Check for number of parameters ***/
    CheckRhs(4, 4);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_old_site_name);
    getVarAddressFromPosition(3, &ptr_new_site_name);
    getVarAddressFromPosition(4, &ptr_new_site_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_old_site_name, &row_old_site_name, &col_old_site_name);
    getVarDimension(ptr_new_site_name, &row_new_site_name, &col_new_site_name);
    getVarDimension(ptr_new_site_func, &row_new_site_func, &col_new_site_func);

    /*** Prerequisites destination ***/
    lengths_old_site_name = (int*) malloc(row_old_site_name * col_old_site_name * sizeof(int));
    getMatrixOfString(ptr_old_site_name, &row_old_site_name, &col_old_site_name, lengths_old_site_name, NULL);
    strings_old_site_name = (char**) malloc(row_old_site_name * col_old_site_name * sizeof(char*));
    for(cnt_old_site_name = 0; cnt_old_site_name < row_old_site_name * col_old_site_name ; ++cnt_old_site_name) strings_old_site_name[cnt_old_site_name] = (char*) malloc((lengths_old_site_name[cnt_old_site_name] + 1) * sizeof(char));
    lengths_new_site_name = (int*) malloc(row_new_site_name * col_new_site_name * sizeof(int));
    getMatrixOfString(ptr_new_site_name, &row_new_site_name, &col_new_site_name, lengths_new_site_name, NULL);
    strings_new_site_name = (char**) malloc(row_new_site_name * col_new_site_name * sizeof(char*));
    for(cnt_new_site_name = 0; cnt_new_site_name < row_new_site_name * col_new_site_name ; ++cnt_new_site_name) strings_new_site_name[cnt_new_site_name] = (char*) malloc((lengths_new_site_name[cnt_new_site_name] + 1) * sizeof(char));
    lengths_new_site_func = (int*) malloc(row_new_site_func * col_new_site_func * sizeof(int));
    getMatrixOfString(ptr_new_site_func, &row_new_site_func, &col_new_site_func, lengths_new_site_func, NULL);
    strings_new_site_func = (char**) malloc(row_new_site_func * col_new_site_func * sizeof(char*));
    for(cnt_new_site_func = 0; cnt_new_site_func < row_new_site_func * col_new_site_func ; ++cnt_new_site_func) strings_new_site_func[cnt_new_site_func] = (char*) malloc((lengths_new_site_func[cnt_new_site_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_old_site_name, &row_old_site_name, &col_old_site_name, lengths_old_site_name, strings_old_site_name);
    getMatrixOfString(ptr_new_site_name, &row_new_site_name, &col_new_site_name, lengths_new_site_name, strings_new_site_name);
    getMatrixOfString(ptr_new_site_func, &row_new_site_func, &col_new_site_func, lengths_new_site_func, strings_new_site_func);

    /*** Parameter copy ***/
    old_site_name = strings_old_site_name[0];
    new_site_name = strings_new_site_name[0];
    new_site_func = strings_new_site_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_changeSiteTableEntry(old_site_name, new_site_name, new_site_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_old_site_name = 0; cnt_old_site_name < row_old_site_name * col_old_site_name ; ++cnt_old_site_name) free(strings_old_site_name[cnt_old_site_name]);
    free(strings_old_site_name);
    free(lengths_old_site_name);
    for(cnt_new_site_name = 0; cnt_new_site_name < row_new_site_name * col_new_site_name ; ++cnt_new_site_name) free(strings_new_site_name[cnt_new_site_name]);
    free(strings_new_site_name);
    free(lengths_new_site_name);
    for(cnt_new_site_func = 0; cnt_new_site_func < row_new_site_func * col_new_site_func ; ++cnt_new_site_func) free(strings_new_site_func[cnt_new_site_func]);
    free(strings_new_site_func);
    free(lengths_new_site_func);


    return 0;
}


int sci_ann_delete_site_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteSiteTableEntry(site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);


    return 0;
}


int sci_ann_set_first_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFirstSite();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_next_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setNextSite();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setSite(site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);


    return 0;
}


int sci_ann_get_site_value(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getSiteValue();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_site_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getSiteName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_site_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setSiteName(site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);


    return 0;
}


int sci_ann_get_site_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getSiteFuncName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_add_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_site_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_site_name = 1, col_site_name = 1;

    /*** Address parameters ***/
    int *ptr_site_name = NULL;
    int cnt_site_name;
    int *lengths_site_name = NULL;
    char **strings_site_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_site_name, &row_site_name, &col_site_name);

    /*** Prerequisites destination ***/
    lengths_site_name = (int*) malloc(row_site_name * col_site_name * sizeof(int));
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, NULL);
    strings_site_name = (char**) malloc(row_site_name * col_site_name * sizeof(char*));
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) strings_site_name[cnt_site_name] = (char*) malloc((lengths_site_name[cnt_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_site_name, &row_site_name, &col_site_name, lengths_site_name, strings_site_name);

    /*** Parameter copy ***/
    site_name = strings_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_addSite(site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_site_name = 0; cnt_site_name < row_site_name * col_site_name ; ++cnt_site_name) free(strings_site_name[cnt_site_name]);
    free(strings_site_name);
    free(lengths_site_name);


    return 0;
}


int sci_ann_delete_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteSite();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


