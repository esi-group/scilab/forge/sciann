#include "stack-c.h"
#include "Scierror.h"
#define POS_LABELS 1
#define POS_STRUCT_POINTER 2
#include "glob_typ.h"
#include "enzo_mem_typ.h"
#include "kr_typ.h"
#include "config.h"
#include "kr_mem.h"
#include "kr_ui.h"

int ctos_pattern_descriptor(unsigned int StackPos, pattern_descriptor *type);
pattern_descriptor * stoc_pattern_descriptor(unsigned int StackPos, int * res);


int ctos_struct_PositionVector(unsigned int StackPos, struct PositionVector *type);
struct PositionVector * stoc_struct_PositionVector(unsigned int StackPos, int * res);


int ctos_memNet(unsigned int StackPos, memNet *type);
memNet * stoc_memNet(unsigned int StackPos, int * res);


int ctos_pattern_set_info(unsigned int StackPos, pattern_set_info *type);
pattern_set_info * stoc_pattern_set_info(unsigned int StackPos, int * res);


int ctos_struct_PosType(unsigned int StackPos, struct PosType *type);
struct PosType * stoc_struct_PosType(unsigned int StackPos, int * res);
