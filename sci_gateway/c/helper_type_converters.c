#include "helper_type_converters.h"

int getMatrixFromDoubleTo_int(int* _piAddress, int* _piRows, int* _piCols, int** _pdblReal)
{
    int i, ret;
    int * table = (int *) malloc(*_piRows * *_piCols * sizeof(int));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the int addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (int) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_int(int _iVar, int _piRows, int _piCols, int* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}


int getMatrixFromDoubleTo_float(int* _piAddress, int* _piRows, int* _piCols, float** _pdblReal)
{
    int i, ret;
    float * table = (float *) malloc(*_piRows * *_piCols * sizeof(float));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the float addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (float) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_float(int _iVar, int _piRows, int _piCols, float* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}


int getMatrixFromDoubleTo_char(int* _piAddress, int* _piRows, int* _piCols, char** _pdblReal)
{
    int i, ret;
    char * table = (char *) malloc(*_piRows * *_piCols * sizeof(char));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the char addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (char) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_char(int _iVar, int _piRows, int _piCols, char* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}


int getMatrixFromDoubleTo_unsigned_short(int* _piAddress, int* _piRows, int* _piCols, unsigned short** _pdblReal)
{
    int i, ret;
    unsigned short * table = (unsigned short *) malloc(*_piRows * *_piCols * sizeof(unsigned short));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the unsigned short addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (unsigned short) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_unsigned_short(int _iVar, int _piRows, int _piCols, unsigned short* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}


int getMatrixFromDoubleTo_long_int(int* _piAddress, int* _piRows, int* _piCols, long int** _pdblReal)
{
    int i, ret;
    long int * table = (long int *) malloc(*_piRows * *_piCols * sizeof(long int));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the long int addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (long int) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_long_int(int _iVar, int _piRows, int _piCols, long int* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}


int getMatrixFromDoubleTo_unsigned_int(int* _piAddress, int* _piRows, int* _piCols, unsigned int** _pdblReal)
{
    int i, ret;
    unsigned int * table = (unsigned int *) malloc(*_piRows * *_piCols * sizeof(unsigned int));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the unsigned int addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (unsigned int) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_unsigned_int(int _iVar, int _piRows, int _piCols, unsigned int* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}
