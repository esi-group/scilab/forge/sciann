#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


bool krui_setFirstFTypeEntry();
bool krui_setNextFTypeEntry();
bool krui_setFTypeEntry(char*);
char* krui_getFTypeName();
krui_err krui_setFTypeName(char*);
char* krui_getFTypeActFuncName();
krui_err krui_setFTypeActFunc(char*);
char* krui_getFTypeOutFuncName();
krui_err krui_setFTypeOutFunc(char*);
bool krui_setFirstFTypeSite();
bool krui_setNextFTypeSite();
char* krui_getFTypeSiteName();
krui_err krui_setFTypeSiteName(char*);
krui_err krui_createFTypeEntry(char*, char*, char*, int, char**);
krui_err krui_deleteFTypeEntry(char*);


int sci_ann_set_first_ftype_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFirstFTypeEntry();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_next_ftype_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setNextFTypeEntry();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_ftype_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ftype_symbol;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* ftype_symbol;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ftype_symbol);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);

    /*** Parameter copy ***/
    ftype_symbol = strings_ftype_symbol[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFTypeEntry(ftype_symbol);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);


    return 0;
}


int sci_ann_get_ftype_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFTypeName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_ftype_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ftype_symbol;

    /*** Dimension parameters ***/
    int res_network;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;

    /*** Address parameters ***/
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;

    /*** Source parameters ***/
    int sci;
    char* ftype_symbol;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ftype_symbol);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);

    /*** Parameter copy ***/
    ftype_symbol = strings_ftype_symbol[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFTypeName(ftype_symbol);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);


    return 0;
}


int sci_ann_get_ftype_act_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFTypeActFuncName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_ftype_act_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_act_func_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_act_func_name = 1, col_act_func_name = 1;

    /*** Address parameters ***/
    int *ptr_act_func_name = NULL;
    int cnt_act_func_name;
    int *lengths_act_func_name = NULL;
    char **strings_act_func_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* act_func_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_act_func_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_act_func_name, &row_act_func_name, &col_act_func_name);

    /*** Prerequisites destination ***/
    lengths_act_func_name = (int*) malloc(row_act_func_name * col_act_func_name * sizeof(int));
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, NULL);
    strings_act_func_name = (char**) malloc(row_act_func_name * col_act_func_name * sizeof(char*));
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) strings_act_func_name[cnt_act_func_name] = (char*) malloc((lengths_act_func_name[cnt_act_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, strings_act_func_name);

    /*** Parameter copy ***/
    act_func_name = strings_act_func_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFTypeActFunc(act_func_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) free(strings_act_func_name[cnt_act_func_name]);
    free(strings_act_func_name);
    free(lengths_act_func_name);


    return 0;
}


int sci_ann_get_ftype_out_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFTypeOutFuncName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_ftype_out_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_out_func_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_out_func_name = 1, col_out_func_name = 1;

    /*** Address parameters ***/
    int *ptr_out_func_name = NULL;
    int cnt_out_func_name;
    int *lengths_out_func_name = NULL;
    char **strings_out_func_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* out_func_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_out_func_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_out_func_name, &row_out_func_name, &col_out_func_name);

    /*** Prerequisites destination ***/
    lengths_out_func_name = (int*) malloc(row_out_func_name * col_out_func_name * sizeof(int));
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, NULL);
    strings_out_func_name = (char**) malloc(row_out_func_name * col_out_func_name * sizeof(char*));
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) strings_out_func_name[cnt_out_func_name] = (char*) malloc((lengths_out_func_name[cnt_out_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, strings_out_func_name);

    /*** Parameter copy ***/
    out_func_name = strings_out_func_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFTypeOutFunc(out_func_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) free(strings_out_func_name[cnt_out_func_name]);
    free(strings_out_func_name);
    free(lengths_out_func_name);


    return 0;
}


int sci_ann_set_first_ftype_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFirstFTypeSite();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_next_ftype_site(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setNextFTypeSite();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_ftype_site_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFTypeSiteName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_ftype_site_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_f_type_site_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_f_type_site_name = 1, col_f_type_site_name = 1;

    /*** Address parameters ***/
    int *ptr_f_type_site_name = NULL;
    int cnt_f_type_site_name;
    int *lengths_f_type_site_name = NULL;
    char **strings_f_type_site_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* f_type_site_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_f_type_site_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_f_type_site_name, &row_f_type_site_name, &col_f_type_site_name);

    /*** Prerequisites destination ***/
    lengths_f_type_site_name = (int*) malloc(row_f_type_site_name * col_f_type_site_name * sizeof(int));
    getMatrixOfString(ptr_f_type_site_name, &row_f_type_site_name, &col_f_type_site_name, lengths_f_type_site_name, NULL);
    strings_f_type_site_name = (char**) malloc(row_f_type_site_name * col_f_type_site_name * sizeof(char*));
    for(cnt_f_type_site_name = 0; cnt_f_type_site_name < row_f_type_site_name * col_f_type_site_name ; ++cnt_f_type_site_name) strings_f_type_site_name[cnt_f_type_site_name] = (char*) malloc((lengths_f_type_site_name[cnt_f_type_site_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_f_type_site_name, &row_f_type_site_name, &col_f_type_site_name, lengths_f_type_site_name, strings_f_type_site_name);

    /*** Parameter copy ***/
    f_type_site_name = strings_f_type_site_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFTypeSiteName(f_type_site_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_f_type_site_name = 0; cnt_f_type_site_name < row_f_type_site_name * col_f_type_site_name ; ++cnt_f_type_site_name) free(strings_f_type_site_name[cnt_f_type_site_name]);
    free(strings_f_type_site_name);
    free(lengths_f_type_site_name);


    return 0;
}


int sci_ann_create_ftype_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ftype_symbol;
    char *var_act_func_name;
    char *var_out_func_name;
    char *var_array_of_site_names;

    /*** Dimension parameters ***/
    int res_network;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;
    int row_act_func_name = 1, col_act_func_name = 1;
    int row_out_func_name = 1, col_out_func_name = 1;
    int row_no_of_sites = 1, col_no_of_sites = 1;
    int row_array_of_site_names = 1, col_array_of_site_names = 1;

    /*** Address parameters ***/
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;
    int *ptr_act_func_name = NULL;
    int cnt_act_func_name;
    int *lengths_act_func_name = NULL;
    char **strings_act_func_name = NULL;
    int *ptr_out_func_name = NULL;
    int cnt_out_func_name;
    int *lengths_out_func_name = NULL;
    char **strings_out_func_name = NULL;
    int *ptr_array_of_site_names = NULL;
    int cnt_array_of_site_names;
    int *lengths_array_of_site_names = NULL;
    char **strings_array_of_site_names = NULL;

    /*** Source parameters ***/
    int sci;
    char* ftype_symbol;
    char* act_func_name;
    char* out_func_name;
    int no_of_sites;
    char** array_of_site_names;

    /*** Check for number of parameters ***/
    CheckRhs(5, 5);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ftype_symbol);
    getVarAddressFromPosition(3, &ptr_act_func_name);
    getVarAddressFromPosition(4, &ptr_out_func_name);
    getVarAddressFromPosition(5, &ptr_array_of_site_names);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);
    getVarDimension(ptr_act_func_name, &row_act_func_name, &col_act_func_name);
    getVarDimension(ptr_out_func_name, &row_out_func_name, &col_out_func_name);
    getVarDimension(ptr_array_of_site_names, &row_array_of_site_names, &col_array_of_site_names);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));
    lengths_act_func_name = (int*) malloc(row_act_func_name * col_act_func_name * sizeof(int));
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, NULL);
    strings_act_func_name = (char**) malloc(row_act_func_name * col_act_func_name * sizeof(char*));
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) strings_act_func_name[cnt_act_func_name] = (char*) malloc((lengths_act_func_name[cnt_act_func_name] + 1) * sizeof(char));
    lengths_out_func_name = (int*) malloc(row_out_func_name * col_out_func_name * sizeof(int));
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, NULL);
    strings_out_func_name = (char**) malloc(row_out_func_name * col_out_func_name * sizeof(char*));
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) strings_out_func_name[cnt_out_func_name] = (char*) malloc((lengths_out_func_name[cnt_out_func_name] + 1) * sizeof(char));
    lengths_array_of_site_names = (int*) malloc(row_array_of_site_names * col_array_of_site_names * sizeof(int));
    getMatrixOfString(ptr_array_of_site_names, &row_array_of_site_names, &col_array_of_site_names, lengths_array_of_site_names, NULL);
    strings_array_of_site_names = (char**) malloc(row_array_of_site_names * col_array_of_site_names * sizeof(char*));
    for(cnt_array_of_site_names = 0; cnt_array_of_site_names < row_array_of_site_names * col_array_of_site_names ; ++cnt_array_of_site_names) strings_array_of_site_names[cnt_array_of_site_names] = (char*) malloc((lengths_array_of_site_names[cnt_array_of_site_names] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, strings_act_func_name);
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, strings_out_func_name);
    getMatrixOfString(ptr_array_of_site_names, &row_array_of_site_names, &col_array_of_site_names, lengths_array_of_site_names, strings_array_of_site_names);

    /*** Parameter copy ***/
    ftype_symbol = strings_ftype_symbol[0];
    act_func_name = strings_act_func_name[0];
    out_func_name = strings_out_func_name[0];
    no_of_sites = col_array_of_site_names;
    array_of_site_names = strings_array_of_site_names;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createFTypeEntry(ftype_symbol, act_func_name, out_func_name, no_of_sites, array_of_site_names);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) free(strings_act_func_name[cnt_act_func_name]);
    free(strings_act_func_name);
    free(lengths_act_func_name);
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) free(strings_out_func_name[cnt_out_func_name]);
    free(strings_out_func_name);
    free(lengths_out_func_name);
    for(cnt_array_of_site_names = 0; cnt_array_of_site_names < row_array_of_site_names * col_array_of_site_names ; ++cnt_array_of_site_names) free(strings_array_of_site_names[cnt_array_of_site_names]);
    free(strings_array_of_site_names);
    free(lengths_array_of_site_names);


    return 0;
}


int sci_ann_delete_ftype_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ftype_symbol;

    /*** Dimension parameters ***/
    int res_network;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;

    /*** Address parameters ***/
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;

    /*** Source parameters ***/
    int sci;
    char* ftype_symbol;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ftype_symbol);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);

    /*** Parameter copy ***/
    ftype_symbol = strings_ftype_symbol[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteFTypeEntry(ftype_symbol);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);


    return 0;
}


