#include "helper_user_types.h"

int ctos_pattern_descriptor(unsigned int StackPos, pattern_descriptor *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"pattern_descriptorlist","pattern_descriptor"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

pattern_descriptor * stoc_pattern_descriptor(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    pattern_descriptor * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"pattern_descriptorlist") != 0) 
    {
      Scierror(999,"Argument 1 is not a pattern_descriptorlist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (pattern_descriptor *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}




int ctos_struct_PositionVector(unsigned int StackPos, struct PositionVector *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"struct_PositionVectorlist","struct_PositionVector"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

struct PositionVector * stoc_struct_PositionVector(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    struct PositionVector * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"struct_PositionVectorlist") != 0) 
    {
      Scierror(999,"Argument 1 is not a struct_PositionVectorlist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (struct PositionVector *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}




int ctos_memNet(unsigned int StackPos, memNet *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"memNetlist","memNet"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

memNet * stoc_memNet(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    memNet * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"memNetlist") != 0) 
    {
      Scierror(999,"Argument 1 is not a memNetlist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (memNet *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}




int ctos_pattern_set_info(unsigned int StackPos, pattern_set_info *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"pattern_set_infolist","pattern_set_info"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

pattern_set_info * stoc_pattern_set_info(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    pattern_set_info * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"pattern_set_infolist") != 0) 
    {
      Scierror(999,"Argument 1 is not a pattern_set_infolist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (pattern_set_info *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}




int ctos_struct_PosType(unsigned int StackPos, struct PosType *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"struct_PosTypelist","struct_PosType"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

struct PosType * stoc_struct_PosType(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    struct PosType * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"struct_PosTypelist") != 0) 
    {
      Scierror(999,"Argument 1 is not a struct_PosTypelist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (struct PosType *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}


