tbx_build_gateway('sciann', ..
                  [ ..
                    'ann_get_no_of_units', 'sci_ann_get_no_of_units'; ..
                    'ann_get_first_unit', 'sci_ann_get_first_unit'; ..
                    'ann_get_next_unit', 'sci_ann_get_next_unit'; ..
                    'ann_get_current_unit', 'sci_ann_get_current_unit'; ..
                    'ann_set_current_unit', 'sci_ann_set_current_unit'; ..
                    'ann_get_unit_name', 'sci_ann_get_unit_name'; ..
                    'ann_set_unit_name', 'sci_ann_set_unit_name'; ..
                    'ann_search_unit_name', 'sci_ann_search_unit_name'; ..
                    'ann_search_next_unit_name', 'sci_ann_search_next_unit_name'; ..
                    'ann_get_unit_out_func_name', 'sci_ann_get_unit_out_func_name'; ..
                    'ann_set_unit_out_func', 'sci_ann_set_unit_out_func'; ..
                    'ann_get_unit_act_func_name', 'sci_ann_get_unit_act_func_name'; ..
                    'ann_set_unit_act_func', 'sci_ann_set_unit_act_func'; ..
                    'ann_get_unit_ftype_name', 'sci_ann_get_unit_ftype_name'; ..
                    'ann_get_unit_activation', 'sci_ann_get_unit_activation'; ..
                    'ann_set_unit_activation', 'sci_ann_set_unit_activation'; ..
                    'ann_get_unit_initial_activation', 'sci_ann_get_unit_initial_activation'; ..
                    'ann_set_unit_initial_activation', 'sci_ann_set_unit_initial_activation'; ..
                    'ann_get_unit_output', 'sci_ann_get_unit_output'; ..
                    'ann_set_unit_output', 'sci_ann_set_unit_output'; ..
                    'ann_get_unit_bias', 'sci_ann_get_unit_bias'; ..
                    'ann_set_unit_bias', 'sci_ann_set_unit_bias'; ..
                    'ann_get_unit_value_a', 'sci_ann_get_unit_value_a'; ..
                    'ann_set_unit_value_a', 'sci_ann_set_unit_value_a'; ..
                    'ann_get_unit_subnet_id', 'sci_ann_get_unit_subnet_id'; ..
                    'ann_set_unit_subnet_id', 'sci_ann_set_unit_subnet_id'; ..
                    'ann_get_unit_layer_id', 'sci_ann_get_unit_layer_id'; ..
                    'ann_set_unit_layer_id', 'sci_ann_set_unit_layer_id'; ..
                    'ann_get_unit_position', 'sci_ann_get_unit_position'; ..
                    'ann_set_unit_position', 'sci_ann_set_unit_position'; ..
                    'ann_get_unit_id_near_position', 'sci_ann_get_unit_id_near_position'; ..
                    'ann_get_unit_ttype', 'sci_ann_get_unit_ttype'; ..
                    'ann_set_unit_ttype', 'sci_ann_set_unit_ttype'; ..
                    'ann_freeze_unit', 'sci_ann_freeze_unit'; ..
                    'ann_unfreeze_unit', 'sci_ann_unfreeze_unit'; ..
                    'ann_is_unit_frozen', 'sci_ann_is_unit_frozen'; ..
                    'ann_get_unit_input_type', 'sci_ann_get_unit_input_type'; ..
                    'ann_create_default_unit', 'sci_ann_create_default_unit'; ..
                    'ann_create_unit', 'sci_ann_create_unit'; ..
                    'ann_delete_unit_list', 'sci_ann_delete_unit_list'; ..
                    'ann_create_ftype_unit', 'sci_ann_create_ftype_unit'; ..
                    'ann_set_unit_ftype', 'sci_ann_set_unit_ftype'; ..
                    'ann_copy_unit', 'sci_ann_copy_unit'; ..
                    'ann_set_first_ftype_entry', 'sci_ann_set_first_ftype_entry'; ..
                    'ann_set_next_ftype_entry', 'sci_ann_set_next_ftype_entry'; ..
                    'ann_set_ftype_entry', 'sci_ann_set_ftype_entry'; ..
                    'ann_get_ftype_name', 'sci_ann_get_ftype_name'; ..
                    'ann_set_ftype_name', 'sci_ann_set_ftype_name'; ..
                    'ann_get_ftype_act_func_name', 'sci_ann_get_ftype_act_func_name'; ..
                    'ann_set_ftype_act_func', 'sci_ann_set_ftype_act_func'; ..
                    'ann_get_ftype_out_func_name', 'sci_ann_get_ftype_out_func_name'; ..
                    'ann_set_ftype_out_func', 'sci_ann_set_ftype_out_func'; ..
                    'ann_set_first_ftype_site', 'sci_ann_set_first_ftype_site'; ..
                    'ann_set_next_ftype_site', 'sci_ann_set_next_ftype_site'; ..
                    'ann_get_ftype_site_name', 'sci_ann_get_ftype_site_name'; ..
                    'ann_set_ftype_site_name', 'sci_ann_set_ftype_site_name'; ..
                    'ann_create_ftype_entry', 'sci_ann_create_ftype_entry'; ..
                    'ann_delete_ftype_entry', 'sci_ann_delete_ftype_entry'; ..
                    'ann_get_no_of_functions', 'sci_ann_get_no_of_functions'; ..
                    'ann_get_func_info', 'sci_ann_get_func_info'; ..
                    'ann_is_function', 'sci_ann_is_function'; ..
                    'ann_get_func_param_info', 'sci_ann_get_func_param_info'; ..
                    'ann_get_first_site_table_entry', 'sci_ann_get_first_site_table_entry'; ..
                    'ann_get_next_site_table_entry', 'sci_ann_get_next_site_table_entry'; ..
                    'ann_get_site_table_func_name', 'sci_ann_get_site_table_func_name'; ..
                    'ann_create_site_table_entry', 'sci_ann_create_site_table_entry'; ..
                    'ann_change_site_table_entry', 'sci_ann_change_site_table_entry'; ..
                    'ann_delete_site_table_entry', 'sci_ann_delete_site_table_entry'; ..
                    'ann_set_first_site', 'sci_ann_set_first_site'; ..
                    'ann_set_next_site', 'sci_ann_set_next_site'; ..
                    'ann_set_site', 'sci_ann_set_site'; ..
                    'ann_get_site_value', 'sci_ann_get_site_value'; ..
                    'ann_get_site_name', 'sci_ann_get_site_name'; ..
                    'ann_set_site_name', 'sci_ann_set_site_name'; ..
                    'ann_get_site_func_name', 'sci_ann_get_site_func_name'; ..
                    'ann_add_site', 'sci_ann_add_site'; ..
                    'ann_delete_site', 'sci_ann_delete_site'; ..
                    'ann_get_first_pred_unit', 'sci_ann_get_first_pred_unit'; ..
                    'ann_get_next_pred_unit', 'sci_ann_get_next_pred_unit'; ..
                    'ann_get_current_pred_unit', 'sci_ann_get_current_pred_unit'; ..
                    'ann_get_first_succ_unit', 'sci_ann_get_first_succ_unit'; ..
                    'ann_get_next_succ_unit', 'sci_ann_get_next_succ_unit'; ..
                    'ann_are_connected', 'sci_ann_are_connected'; ..
                    'ann_are_connected_weight', 'sci_ann_are_connected_weight'; ..
                    'ann_is_connected', 'sci_ann_is_connected'; ..
                    'ann_get_link_weight', 'sci_ann_get_link_weight'; ..
                    'ann_set_link_weight', 'sci_ann_set_link_weight'; ..
                    'ann_create_link', 'sci_ann_create_link'; ..
                    'ann_delete_link', 'sci_ann_delete_link'; ..
                    'ann_delete_all_input_links', 'sci_ann_delete_all_input_links'; ..
                    'ann_delete_all_output_links', 'sci_ann_delete_all_output_links'; ..
                    'ann_jog_weights', 'sci_ann_jog_weights'; ..
                    'ann_jog_corr_weights', 'sci_ann_jog_corr_weights'; ..
                    'ann_get_variance', 'sci_ann_get_variance'; ..
                    'ann_count_links', 'sci_ann_count_links'; ..
                    'ann_update_single_unit', 'sci_ann_update_single_unit'; ..
                    'ann_get_update_func', 'sci_ann_get_update_func'; ..
                    'ann_set_remap_func', 'sci_ann_set_remap_func'; ..
                    'ann_set_update_func', 'sci_ann_set_update_func'; ..
                    'ann_update_net', 'sci_ann_update_net'; ..
                    'ann_get_initialization_func', 'sci_ann_get_initialization_func'; ..
                    'ann_set_initialization_func', 'sci_ann_set_initialization_func'; ..
                    'ann_initialize_net', 'sci_ann_initialize_net'; ..
                    'ann_get_learn_func', 'sci_ann_get_learn_func'; ..
                    'ann_set_learn_func', 'sci_ann_set_learn_func'; ..
                    'ann_check_pruning', 'sci_ann_check_pruning'; ..
                    'ann_learn_all_patterns', 'sci_ann_learn_all_patterns'; ..
                    'ann_test_all_patterns', 'sci_ann_test_all_patterns'; ..
                    'ann_learn_single_pattern', 'sci_ann_learn_single_pattern'; ..
                    'ann_test_single_pattern', 'sci_ann_test_single_pattern'; ..
                    'ann_learn_all_patterns_ff', 'sci_ann_learn_all_patterns_ff'; ..
                    'ann_learn_single_pattern_ff', 'sci_ann_learn_single_pattern_ff'; ..
                    'ann_get_prun_func', 'sci_ann_get_prun_func'; ..
                    'ann_set_prun_func', 'sci_ann_set_prun_func'; ..
                    'ann_get_ff_learn_func', 'sci_ann_get_ff_learn_func'; ..
                    'ann_set_ff_learn_func', 'sci_ann_set_ff_learn_func'; ..
                    'ann_set_class_distribution', 'sci_ann_set_class_distribution'; ..
                    'ann_set_class_info', 'sci_ann_set_class_info'; ..
                    'ann_use_class_distribution', 'sci_ann_use_class_distribution'; ..
                    'ann_set_pattern_id', 'sci_ann_set_pattern_id'; ..
                    'ann_get_pattern_id', 'sci_ann_get_pattern_id'; ..
                    'ann_delete_pattern', 'sci_ann_delete_pattern'; ..
                    'ann_modify_pattern', 'sci_ann_modify_pattern'; ..
                    'ann_show_pattern', 'sci_ann_show_pattern'; ..
                    'ann_alloc_new_pattern_set', 'sci_ann_alloc_new_pattern_set'; ..
                    'ann_new_pattern', 'sci_ann_new_pattern'; ..
                    'ann_get_no_of_patterns', 'sci_ann_get_no_of_patterns'; ..
                    'ann_get_total_no_of_sub_patterns', 'sci_ann_get_total_no_of_sub_patterns'; ..
                    'ann_delete_all_patterns', 'sci_ann_delete_all_patterns'; ..
                    'ann_shuffle_patterns', 'sci_ann_shuffle_patterns'; ..
                    'ann_shuffle_sub_patterns', 'sci_ann_shuffle_sub_patterns'; ..
                    'ann_set_curr_pat_set', 'sci_ann_set_curr_pat_set'; ..
                    'ann_delete_pat_set', 'sci_ann_delete_pat_set'; ..
                    'ann_get_pat_info', 'sci_ann_get_pat_info'; ..
                    'ann_def_show_sub_pat', 'sci_ann_def_show_sub_pat'; ..
                    'ann_def_train_sub_pat', 'sci_ann_def_train_sub_pat'; ..
                    'ann_align_sub_pat', 'sci_ann_align_sub_pat'; ..
                    'ann_get_shape_of_sub_pattern', 'sci_ann_get_shape_of_sub_pattern'; ..
                    'ann_save_net', 'sci_ann_save_net'; ..
                    'ann_load_net', 'sci_ann_load_net'; ..
                    'ann_load_new_patterns', 'sci_ann_load_new_patterns'; ..
                    'ann_save_new_patterns', 'sci_ann_save_new_patterns'; ..
                    'ann_save_result_param', 'sci_ann_save_result_param'; ..
                    'ann_allocate_units', 'sci_ann_allocate_units'; ..
                    'ann_delete_net', 'sci_ann_delete_net'; ..
                    'ann_get_first_symbol_table_entry', 'sci_ann_get_first_symbol_table_entry'; ..
                    'ann_get_next_symbol_table_entry', 'sci_ann_get_next_symbol_table_entry'; ..
                    'ann_symbol_search', 'sci_ann_symbol_search'; ..
                    'ann_get_version', 'sci_ann_get_version'; ..
                    'ann_get_net_info', 'sci_ann_get_net_info'; ..
                    'ann_get_memory_manager_info', 'sci_ann_get_memory_manager_info'; ..
                    'ann_get_unit_defaults', 'sci_ann_get_unit_defaults'; ..
                    'ann_set_unit_defaults', 'sci_ann_set_unit_defaults'; ..
                    'ann_reset_net', 'sci_ann_reset_net'; ..
                    'ann_set_seed_no', 'sci_ann_set_seed_no'; ..
                    'ann_get_no_of_input_units', 'sci_ann_get_no_of_input_units'; ..
                    'ann_get_no_of_output_units', 'sci_ann_get_no_of_output_units'; ..
                    'ann_get_no_of_ttype_units', 'sci_ann_get_no_of_ttype_units'; ..
                    'ann_get_no_of_special_input_units', 'sci_ann_get_no_of_special_input_units'; ..
                    'ann_get_no_of_special_output_units', 'sci_ann_get_no_of_special_output_units'; ..
                    'ann_xy_trans_table', 'sci_ann_xy_trans_table'; ..
                    'ann_get_unit_centers', 'sci_ann_get_unit_centers'; ..
                    'ann_set_unit_centers', 'sci_ann_set_unit_centers'; ..

                  ], ..
                  [ ..
                    'ann_links.c', ..
                    'ann_prototypes.c', ..
                    'ann_io.c', ..
                    'gateway.c', ..
                    'ann_ftable.c', ..
                    'ann_learning.c', ..
                    'ann_sites.c', ..
                    'ann_units.c', ..
                    'ann_pattern.c', ..
                    'ann_init.c', ..
                    'ann_memory.c', ..
                    'ann_activation.c', ..
                    'ann_stable.c', ..
                    'helper_user_types.c', ..
                    'helper_type_converters.c', ..
                    'additional.c', ..

                  ], ..
                  get_absolute_file_path('builder_gateway_c.sce'), ..
                  ['../../src/c/libsnns'], '', '-I' + get_absolute_file_path('builder_gateway_c.sce') + ' -I' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c/');

clear tbx_build_gateway;
