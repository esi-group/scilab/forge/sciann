#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


int krui_getNoOfUnits();
int krui_getFirstUnit();
int krui_getNextUnit();
int krui_getCurrentUnit();
krui_err krui_setCurrentUnit(int);
char* krui_getUnitName(int);
krui_err krui_setUnitName(int, char*);
int krui_searchUnitName(char*);
int krui_searchNextUnitName();
char* krui_getUnitOutFuncName(int);
krui_err krui_setUnitOutFunc(int, char*);
char* krui_getUnitActFuncName(int);
krui_err krui_setUnitActFunc(int, char*);
char* krui_getUnitFTypeName(int);
FlintType krui_getUnitActivation(int);
krui_err krui_setUnitActivation(int, FlintTypeParam);
FlintType krui_getUnitInitialActivation(int);
void krui_setUnitInitialActivation(int, FlintTypeParam);
FlintType krui_getUnitOutput(int);
krui_err krui_setUnitOutput(int, FlintTypeParam);
FlintType krui_getUnitBias(int);
void krui_setUnitBias(int, FlintTypeParam);
FlintType krui_getUnitValueA(int);
void krui_setUnitValueA(int, FlintTypeParam);
int krui_getUnitSubnetNo(int);
void krui_setUnitSubnetNo(int, int);
unsigned short krui_getUnitLayerNo(int);
void krui_setUnitLayerNo(int, int);
void krui_getUnitPosition(int, struct PosType*);
void krui_setUnitPosition(int, struct PosType*);
int krui_getUnitNoNearPosition(struct PosType*, int, int, int);
int krui_getUnitTType(int);
krui_err krui_setUnitTType(int, int);
krui_err krui_freezeUnit(int);
krui_err krui_unfreezeUnit(int);
bool krui_isUnitFrozen(int);
int krui_getUnitInputType(int);
int krui_createDefaultUnit();
int krui_createUnit(char*, char*, char*, FlintTypeParam, FlintTypeParam);
krui_err krui_deleteUnitList(int, int*);
int krui_createFTypeUnit(char*);
krui_err krui_setUnitFType(int, char*);
int krui_copyUnit(int, int);
krui_err krui_allocateUnits(int);
int krui_getNoOfTTypeUnits(int);
krui_err krui_xyTransTable(int, int*, int*, int);
krui_err krui_getUnitCenters(int, int, struct PositionVector**);
krui_err krui_setUnitCenters(int, int, struct PositionVector*);


int sci_ann_get_no_of_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfUnits();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_first_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFirstUnit();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_next_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNextUnit();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_current_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getCurrentUnit();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_current_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setCurrentUnit(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_unit_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitName(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(3, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_unit_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_unit_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_name = 1, col_unit_name = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_name = NULL;
    int cnt_unit_name;
    int *lengths_unit_name = NULL;
    char **strings_unit_name = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    char* unit_name;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_name, &row_unit_name, &col_unit_name);

    /*** Prerequisites destination ***/
    lengths_unit_name = (int*) malloc(row_unit_name * col_unit_name * sizeof(int));
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, NULL);
    strings_unit_name = (char**) malloc(row_unit_name * col_unit_name * sizeof(char*));
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) strings_unit_name[cnt_unit_name] = (char*) malloc((lengths_unit_name[cnt_unit_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, strings_unit_name);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_name = strings_unit_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitName(unit_id, unit_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) free(strings_unit_name[cnt_unit_name]);
    free(strings_unit_name);
    free(lengths_unit_name);


    return 0;
}


int sci_ann_search_unit_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_unit_name;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_name = 1, col_unit_name = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_name = NULL;
    int cnt_unit_name;
    int *lengths_unit_name = NULL;
    char **strings_unit_name = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* unit_name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_name, &row_unit_name, &col_unit_name);

    /*** Prerequisites destination ***/
    lengths_unit_name = (int*) malloc(row_unit_name * col_unit_name * sizeof(int));
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, NULL);
    strings_unit_name = (char**) malloc(row_unit_name * col_unit_name * sizeof(char*));
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) strings_unit_name[cnt_unit_name] = (char*) malloc((lengths_unit_name[cnt_unit_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, strings_unit_name);

    /*** Parameter copy ***/
    unit_name = strings_unit_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_searchUnitName(unit_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites destination ***/
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) free(strings_unit_name[cnt_unit_name]);
    free(strings_unit_name);
    free(lengths_unit_name);


    return 0;
}


int sci_ann_search_next_unit_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_searchNextUnitName();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_unit_out_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitOutFuncName(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(3, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_unit_out_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_unit_out_func_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_out_func_name = 1, col_unit_out_func_name = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_out_func_name = NULL;
    int cnt_unit_out_func_name;
    int *lengths_unit_out_func_name = NULL;
    char **strings_unit_out_func_name = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    char* unit_out_func_name;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_out_func_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_out_func_name, &row_unit_out_func_name, &col_unit_out_func_name);

    /*** Prerequisites destination ***/
    lengths_unit_out_func_name = (int*) malloc(row_unit_out_func_name * col_unit_out_func_name * sizeof(int));
    getMatrixOfString(ptr_unit_out_func_name, &row_unit_out_func_name, &col_unit_out_func_name, lengths_unit_out_func_name, NULL);
    strings_unit_out_func_name = (char**) malloc(row_unit_out_func_name * col_unit_out_func_name * sizeof(char*));
    for(cnt_unit_out_func_name = 0; cnt_unit_out_func_name < row_unit_out_func_name * col_unit_out_func_name ; ++cnt_unit_out_func_name) strings_unit_out_func_name[cnt_unit_out_func_name] = (char*) malloc((lengths_unit_out_func_name[cnt_unit_out_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfString(ptr_unit_out_func_name, &row_unit_out_func_name, &col_unit_out_func_name, lengths_unit_out_func_name, strings_unit_out_func_name);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_out_func_name = strings_unit_out_func_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitOutFunc(unit_id, unit_out_func_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_unit_out_func_name = 0; cnt_unit_out_func_name < row_unit_out_func_name * col_unit_out_func_name ; ++cnt_unit_out_func_name) free(strings_unit_out_func_name[cnt_unit_out_func_name]);
    free(strings_unit_out_func_name);
    free(lengths_unit_out_func_name);


    return 0;
}


int sci_ann_get_unit_act_func_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitActFuncName(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(3, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_unit_act_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_unit_act_func_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_act_func_name = 1, col_unit_act_func_name = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_act_func_name = NULL;
    int cnt_unit_act_func_name;
    int *lengths_unit_act_func_name = NULL;
    char **strings_unit_act_func_name = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    char* unit_act_func_name;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_act_func_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_act_func_name, &row_unit_act_func_name, &col_unit_act_func_name);

    /*** Prerequisites destination ***/
    lengths_unit_act_func_name = (int*) malloc(row_unit_act_func_name * col_unit_act_func_name * sizeof(int));
    getMatrixOfString(ptr_unit_act_func_name, &row_unit_act_func_name, &col_unit_act_func_name, lengths_unit_act_func_name, NULL);
    strings_unit_act_func_name = (char**) malloc(row_unit_act_func_name * col_unit_act_func_name * sizeof(char*));
    for(cnt_unit_act_func_name = 0; cnt_unit_act_func_name < row_unit_act_func_name * col_unit_act_func_name ; ++cnt_unit_act_func_name) strings_unit_act_func_name[cnt_unit_act_func_name] = (char*) malloc((lengths_unit_act_func_name[cnt_unit_act_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfString(ptr_unit_act_func_name, &row_unit_act_func_name, &col_unit_act_func_name, lengths_unit_act_func_name, strings_unit_act_func_name);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_act_func_name = strings_unit_act_func_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitActFunc(unit_id, unit_act_func_name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_unit_act_func_name = 0; cnt_unit_act_func_name < row_unit_act_func_name * col_unit_act_func_name ; ++cnt_unit_act_func_name) free(strings_unit_act_func_name[cnt_unit_act_func_name]);
    free(strings_unit_act_func_name);
    free(lengths_unit_act_func_name);


    return 0;
}


int sci_ann_get_unit_ftype_name(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitFTypeName(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(3, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_get_unit_activation(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitActivation(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_activation(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_unit_activation;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_activation = 1, col_unit_activation = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_activation = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    double unit_activation;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_activation);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_activation, &row_unit_activation, &col_unit_activation);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfDouble(ptr_unit_activation, &row_unit_activation, &col_unit_activation, &var_unit_activation);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_activation = *var_unit_activation;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitActivation(unit_id, unit_activation);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_unit_initial_activation(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitInitialActivation(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_initial_activation(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_unit_i_activation;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_i_activation = 1, col_unit_i_activation = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_i_activation = NULL;

    /*** Source parameters ***/
    int unit_id;
    double unit_i_activation;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_i_activation);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_i_activation, &row_unit_i_activation, &col_unit_i_activation);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfDouble(ptr_unit_i_activation, &row_unit_i_activation, &col_unit_i_activation, &var_unit_i_activation);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_i_activation = *var_unit_i_activation;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitInitialActivation(unit_id, unit_i_activation);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_output(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitOutput(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_output(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_unit_output;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_output = 1, col_unit_output = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_output = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    double unit_output;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_output);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_output, &row_unit_output, &col_unit_output);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfDouble(ptr_unit_output, &row_unit_output, &col_unit_output, &var_unit_output);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_output = *var_unit_output;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitOutput(unit_id, unit_output);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_unit_bias(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitBias(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_bias(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_unit_bias;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_bias = 1, col_unit_bias = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_bias = NULL;

    /*** Source parameters ***/
    int unit_id;
    double unit_bias;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_bias);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_bias, &row_unit_bias, &col_unit_bias);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfDouble(ptr_unit_bias, &row_unit_bias, &col_unit_bias, &var_unit_bias);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_bias = *var_unit_bias;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitBias(unit_id, unit_bias);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_value_a(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitValueA(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_value_a(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    double *var_unit_value_a;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_value_a = 1, col_unit_value_a = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_value_a = NULL;

    /*** Source parameters ***/
    int unit_id;
    double unit_value_a;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_value_a);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_value_a, &row_unit_value_a, &col_unit_value_a);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfDouble(ptr_unit_value_a, &row_unit_value_a, &col_unit_value_a, &var_unit_value_a);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_value_a = *var_unit_value_a;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitValueA(unit_id, unit_value_a);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_subnet_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitSubnetNo(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_subnet_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_subnet_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_subnet_id = 1, col_subnet_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_subnet_id = NULL;

    /*** Source parameters ***/
    int unit_id;
    int subnet_id;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_subnet_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_subnet_id, &row_subnet_id, &col_subnet_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_subnet_id, &row_subnet_id, &col_subnet_id, &var_subnet_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    subnet_id = *var_subnet_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitSubnetNo(unit_id, subnet_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_layer_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    unsigned short *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    unsigned short sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitLayerNo(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_unsigned_short(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_layer_id(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_layer_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_layer_id = 1, col_layer_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_layer_id = NULL;

    /*** Source parameters ***/
    int unit_id;
    int layer_id;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_layer_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_layer_id, &row_layer_id, &col_layer_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_layer_id, &row_layer_id, &col_layer_id, &var_layer_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    layer_id = *var_layer_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitLayerNo(unit_id, layer_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_position(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    struct PosType *var_position_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int res_position;
    int res_position_ret;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int unit_id;
    struct PosType* position;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_getUnitPosition(unit_id, position);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Copy return parameters ***/
    var_position_ret = position;

    /*** Create return parameters ***/
    res_position_ret = ctos_struct_PosType(3, var_position_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_position(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    struct PosType *var_position;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int res_position;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int unit_id;
    struct PosType* position;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    var_position = stoc_struct_PosType(3, &res_position);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    position = var_position;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setUnitPosition(unit_id, position);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_unit_id_near_position(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    struct PosType *var_position;
    int *var_subnet_id;
    int *var_range;
    int *var_grid_width;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int res_position;
    int row_subnet_id = 1, col_subnet_id = 1;
    int row_range = 1, col_range = 1;
    int row_grid_width = 1, col_grid_width = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_subnet_id = NULL;
    int *ptr_range = NULL;
    int *ptr_grid_width = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    struct PosType* position;
    int subnet_id;
    int range;
    int grid_width;

    /*** Check for number of parameters ***/
    CheckRhs(5, 5);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    var_position = stoc_struct_PosType(2, &res_position);
    getVarAddressFromPosition(3, &ptr_subnet_id);
    getVarAddressFromPosition(4, &ptr_range);
    getVarAddressFromPosition(5, &ptr_grid_width);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_subnet_id, &row_subnet_id, &col_subnet_id);
    getVarDimension(ptr_range, &row_range, &col_range);
    getVarDimension(ptr_grid_width, &row_grid_width, &col_grid_width);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_subnet_id, &row_subnet_id, &col_subnet_id, &var_subnet_id);
    getMatrixFromDoubleTo_int(ptr_range, &row_range, &col_range, &var_range);
    getMatrixFromDoubleTo_int(ptr_grid_width, &row_grid_width, &col_grid_width, &var_grid_width);

    /*** Parameter copy ***/
    position = var_position;
    subnet_id = *var_subnet_id;
    range = *var_range;
    grid_width = *var_grid_width;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitNoNearPosition(position, subnet_id, range, grid_width);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(6, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 6;


    return 0;
}


int sci_ann_get_unit_ttype(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitTType(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_set_unit_ttype(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_unit_ttype;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_unit_ttype = 1, col_unit_ttype = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_unit_ttype = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    int unit_ttype;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_unit_ttype);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_unit_ttype, &row_unit_ttype, &col_unit_ttype);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_unit_ttype, &row_unit_ttype, &col_unit_ttype, &var_unit_ttype);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    unit_ttype = *var_unit_ttype;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitTType(unit_id, unit_ttype);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_freeze_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_freezeUnit(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_unfreeze_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_unfreezeUnit(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_is_unit_frozen(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_isUnitFrozen(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_get_unit_input_type(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitInputType(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_create_default_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createDefaultUnit();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_create_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_unit_name;
    char *var_out_func_name;
    char *var_act_func_name;
    double *var_i_act;
    double *var_bias;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_name = 1, col_unit_name = 1;
    int row_out_func_name = 1, col_out_func_name = 1;
    int row_act_func_name = 1, col_act_func_name = 1;
    int row_i_act = 1, col_i_act = 1;
    int row_bias = 1, col_bias = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_name = NULL;
    int cnt_unit_name;
    int *lengths_unit_name = NULL;
    char **strings_unit_name = NULL;
    int *ptr_out_func_name = NULL;
    int cnt_out_func_name;
    int *lengths_out_func_name = NULL;
    char **strings_out_func_name = NULL;
    int *ptr_act_func_name = NULL;
    int cnt_act_func_name;
    int *lengths_act_func_name = NULL;
    char **strings_act_func_name = NULL;
    int *ptr_i_act = NULL;
    int *ptr_bias = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* unit_name;
    char* out_func_name;
    char* act_func_name;
    double i_act;
    double bias;

    /*** Check for number of parameters ***/
    CheckRhs(6, 6);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_name);
    getVarAddressFromPosition(3, &ptr_out_func_name);
    getVarAddressFromPosition(4, &ptr_act_func_name);
    getVarAddressFromPosition(5, &ptr_i_act);
    getVarAddressFromPosition(6, &ptr_bias);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_name, &row_unit_name, &col_unit_name);
    getVarDimension(ptr_out_func_name, &row_out_func_name, &col_out_func_name);
    getVarDimension(ptr_act_func_name, &row_act_func_name, &col_act_func_name);
    getVarDimension(ptr_i_act, &row_i_act, &col_i_act);
    getVarDimension(ptr_bias, &row_bias, &col_bias);

    /*** Prerequisites destination ***/
    lengths_unit_name = (int*) malloc(row_unit_name * col_unit_name * sizeof(int));
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, NULL);
    strings_unit_name = (char**) malloc(row_unit_name * col_unit_name * sizeof(char*));
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) strings_unit_name[cnt_unit_name] = (char*) malloc((lengths_unit_name[cnt_unit_name] + 1) * sizeof(char));
    lengths_out_func_name = (int*) malloc(row_out_func_name * col_out_func_name * sizeof(int));
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, NULL);
    strings_out_func_name = (char**) malloc(row_out_func_name * col_out_func_name * sizeof(char*));
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) strings_out_func_name[cnt_out_func_name] = (char*) malloc((lengths_out_func_name[cnt_out_func_name] + 1) * sizeof(char));
    lengths_act_func_name = (int*) malloc(row_act_func_name * col_act_func_name * sizeof(int));
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, NULL);
    strings_act_func_name = (char**) malloc(row_act_func_name * col_act_func_name * sizeof(char*));
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) strings_act_func_name[cnt_act_func_name] = (char*) malloc((lengths_act_func_name[cnt_act_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_unit_name, &row_unit_name, &col_unit_name, lengths_unit_name, strings_unit_name);
    getMatrixOfString(ptr_out_func_name, &row_out_func_name, &col_out_func_name, lengths_out_func_name, strings_out_func_name);
    getMatrixOfString(ptr_act_func_name, &row_act_func_name, &col_act_func_name, lengths_act_func_name, strings_act_func_name);
    getMatrixOfDouble(ptr_i_act, &row_i_act, &col_i_act, &var_i_act);
    getMatrixOfDouble(ptr_bias, &row_bias, &col_bias, &var_bias);

    /*** Parameter copy ***/
    unit_name = strings_unit_name[0];
    out_func_name = strings_out_func_name[0];
    act_func_name = strings_act_func_name[0];
    i_act = *var_i_act;
    bias = *var_bias;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createUnit(unit_name, out_func_name, act_func_name, i_act, bias);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(7, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 7;

    /*** Postrequisites destination ***/
    for(cnt_unit_name = 0; cnt_unit_name < row_unit_name * col_unit_name ; ++cnt_unit_name) free(strings_unit_name[cnt_unit_name]);
    free(strings_unit_name);
    free(lengths_unit_name);
    for(cnt_out_func_name = 0; cnt_out_func_name < row_out_func_name * col_out_func_name ; ++cnt_out_func_name) free(strings_out_func_name[cnt_out_func_name]);
    free(strings_out_func_name);
    free(lengths_out_func_name);
    for(cnt_act_func_name = 0; cnt_act_func_name < row_act_func_name * col_act_func_name ; ++cnt_act_func_name) free(strings_act_func_name[cnt_act_func_name]);
    free(strings_act_func_name);
    free(lengths_act_func_name);


    return 0;
}


int sci_ann_delete_unit_list(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_list;

    /*** Dimension parameters ***/
    int res_network;
    int row_no_of_units = 1, col_no_of_units = 1;
    int row_unit_list = 1, col_unit_list = 1;

    /*** Address parameters ***/
    int *ptr_unit_list = NULL;

    /*** Source parameters ***/
    int sci;
    int no_of_units;
    int* unit_list;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_list);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_list, &row_unit_list, &col_unit_list);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_list, &row_unit_list, &col_unit_list, &var_unit_list);

    /*** Parameter copy ***/
    no_of_units = col_unit_list;
    unit_list = var_unit_list;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteUnitList(no_of_units, unit_list);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_create_ftype_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ftype_symbol;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* ftype_symbol;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ftype_symbol);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);

    /*** Parameter copy ***/
    ftype_symbol = strings_ftype_symbol[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createFTypeUnit(ftype_symbol);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);


    return 0;
}


int sci_ann_set_unit_ftype(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    char *var_ftype_symbol;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_ftype_symbol = 1, col_ftype_symbol = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_ftype_symbol = NULL;
    int cnt_ftype_symbol;
    int *lengths_ftype_symbol = NULL;
    char **strings_ftype_symbol = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    char* ftype_symbol;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_ftype_symbol);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol);

    /*** Prerequisites destination ***/
    lengths_ftype_symbol = (int*) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(int));
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, NULL);
    strings_ftype_symbol = (char**) malloc(row_ftype_symbol * col_ftype_symbol * sizeof(char*));
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) strings_ftype_symbol[cnt_ftype_symbol] = (char*) malloc((lengths_ftype_symbol[cnt_ftype_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixOfString(ptr_ftype_symbol, &row_ftype_symbol, &col_ftype_symbol, lengths_ftype_symbol, strings_ftype_symbol);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    ftype_symbol = strings_ftype_symbol[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitFType(unit_id, ftype_symbol);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_ftype_symbol = 0; cnt_ftype_symbol < row_ftype_symbol * col_ftype_symbol ; ++cnt_ftype_symbol) free(strings_ftype_symbol[cnt_ftype_symbol]);
    free(strings_ftype_symbol);
    free(lengths_ftype_symbol);


    return 0;
}


int sci_ann_copy_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_copy_mode;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_copy_mode = 1, col_copy_mode = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_copy_mode = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    int copy_mode;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_copy_mode);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_copy_mode, &row_copy_mode, &col_copy_mode);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_copy_mode, &row_copy_mode, &col_copy_mode, &var_copy_mode);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    copy_mode = *var_copy_mode;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_copyUnit(unit_id, copy_mode);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;


    return 0;
}


int sci_ann_allocate_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_no_of_units;

    /*** Dimension parameters ***/
    int res_network;
    int row_no_of_units = 1, col_no_of_units = 1;

    /*** Address parameters ***/
    int *ptr_no_of_units = NULL;

    /*** Source parameters ***/
    int sci;
    int no_of_units;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_no_of_units);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_no_of_units, &row_no_of_units, &col_no_of_units);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_no_of_units, &row_no_of_units, &col_no_of_units, &var_no_of_units);

    /*** Parameter copy ***/
    no_of_units = *var_no_of_units;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_allocateUnits(no_of_units);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_no_of_ttype_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_ttype;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_ttype = 1, col_unit_ttype = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_ttype = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_ttype;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_ttype);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_ttype, &row_unit_ttype, &col_unit_ttype);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_ttype, &row_unit_ttype, &col_unit_ttype, &var_unit_ttype);

    /*** Parameter copy ***/
    unit_ttype = *var_unit_ttype;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfTTypeUnits(unit_ttype);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_xy_trans_table(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_op;
    int *var_x;
    int *var_y;
    int *var_z;
    int *var_x_ret;
    int *var_y_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_op = 1, col_op = 1;
    int row_x = 1, col_x = 1;
    int row_y = 1, col_y = 1;
    int row_z = 1, col_z = 1;
    int row_x_ret = 1, col_x_ret = 1;
    int row_y_ret = 1, col_y_ret = 1;

    /*** Address parameters ***/
    int *ptr_op = NULL;
    int *ptr_x = NULL;
    int *ptr_y = NULL;
    int *ptr_z = NULL;
    int *ptr_x_ret = NULL;
    int *ptr_y_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int op;
    int* x;
    int* y;
    int z;

    /*** Check for number of parameters ***/
    CheckRhs(5, 5);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_op);
    getVarAddressFromPosition(3, &ptr_x);
    getVarAddressFromPosition(4, &ptr_y);
    getVarAddressFromPosition(5, &ptr_z);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_op, &row_op, &col_op);
    getVarDimension(ptr_x, &row_x, &col_x);
    getVarDimension(ptr_y, &row_y, &col_y);
    getVarDimension(ptr_z, &row_z, &col_z);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_op, &row_op, &col_op, &var_op);
    getMatrixFromDoubleTo_int(ptr_x, &row_x, &col_x, &var_x);
    getMatrixFromDoubleTo_int(ptr_y, &row_y, &col_y, &var_y);
    getMatrixFromDoubleTo_int(ptr_z, &row_z, &col_z, &var_z);

    /*** Parameter copy ***/
    op = *var_op;
    x = var_x;
    y = var_y;
    z = *var_z;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_xyTransTable(op, x, y, z);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(6, row_x_ret, col_x_ret, x);
    createMatrixOfDoubleFrom_int(7, row_y_ret, col_y_ret, y);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 6;
    LhsVar(2) = 7;


    return 0;
}


int sci_ann_get_unit_centers(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_center_id;
    struct PositionVector *var_unit_center_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_center_id = 1, col_center_id = 1;
    int res_unit_center;
    int res_unit_center_ret;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_center_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    int center_id;
    struct PositionVector** unit_center;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_center_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_center_id, &row_center_id, &col_center_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_center_id, &row_center_id, &col_center_id, &var_center_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    center_id = *var_center_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUnitCenters(unit_id, center_id, unit_center);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    var_unit_center_ret = *unit_center;

    /*** Create return parameters ***/
    res_unit_center_ret = ctos_struct_PositionVector(4, var_unit_center_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;


    return 0;
}


int sci_ann_set_unit_centers(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;
    int *var_center_id;
    struct PositionVector *var_unit_center;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;
    int row_center_id = 1, col_center_id = 1;
    int res_unit_center;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;
    int *ptr_center_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;
    int center_id;
    struct PositionVector* unit_center;

    /*** Check for number of parameters ***/
    CheckRhs(4, 4);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);
    getVarAddressFromPosition(3, &ptr_center_id);
    var_unit_center = stoc_struct_PositionVector(4, &res_unit_center);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);
    getVarDimension(ptr_center_id, &row_center_id, &col_center_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);
    getMatrixFromDoubleTo_int(ptr_center_id, &row_center_id, &col_center_id, &var_center_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;
    center_id = *var_center_id;
    unit_center = var_unit_center;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitCenters(unit_id, center_id, unit_center);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


