#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


char* krui_getInitialisationFunc();
krui_err krui_setInitialisationFunc(char*);
krui_err krui_initializeNet(float*, int);


int sci_ann_get_initialization_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getInitialisationFunc();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_initialization_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_initialisation_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_initialisation_func = 1, col_initialisation_func = 1;

    /*** Address parameters ***/
    int *ptr_initialisation_func = NULL;
    int cnt_initialisation_func;
    int *lengths_initialisation_func = NULL;
    char **strings_initialisation_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* initialisation_func;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_initialisation_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_initialisation_func, &row_initialisation_func, &col_initialisation_func);

    /*** Prerequisites destination ***/
    lengths_initialisation_func = (int*) malloc(row_initialisation_func * col_initialisation_func * sizeof(int));
    getMatrixOfString(ptr_initialisation_func, &row_initialisation_func, &col_initialisation_func, lengths_initialisation_func, NULL);
    strings_initialisation_func = (char**) malloc(row_initialisation_func * col_initialisation_func * sizeof(char*));
    for(cnt_initialisation_func = 0; cnt_initialisation_func < row_initialisation_func * col_initialisation_func ; ++cnt_initialisation_func) strings_initialisation_func[cnt_initialisation_func] = (char*) malloc((lengths_initialisation_func[cnt_initialisation_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_initialisation_func, &row_initialisation_func, &col_initialisation_func, lengths_initialisation_func, strings_initialisation_func);

    /*** Parameter copy ***/
    initialisation_func = strings_initialisation_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setInitialisationFunc(initialisation_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_initialisation_func = 0; cnt_initialisation_func < row_initialisation_func * col_initialisation_func ; ++cnt_initialisation_func) free(strings_initialisation_func[cnt_initialisation_func]);
    free(strings_initialisation_func);
    free(lengths_initialisation_func);


    return 0;
}


int sci_ann_initialize_net(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_parameter_in_array;

    /*** Dimension parameters ***/
    int res_network;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;

    /*** Address parameters ***/
    int *ptr_parameter_in_array = NULL;

    /*** Source parameters ***/
    int sci;
    float* parameter_in_array;
    int no_of_in_params;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Parameter copy ***/
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_initializeNet(parameter_in_array, no_of_in_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


