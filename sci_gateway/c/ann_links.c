#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


int krui_getFirstPredUnit(FlintType*);
int krui_getNextPredUnit(FlintType*);
int krui_getCurrentPredUnit(FlintType*);
int krui_getFirstSuccUnit(int, FlintType*);
int krui_getNextSuccUnit(FlintType*);
bool krui_areConnected(int, int);
bool krui_areConnectedWeight(int, int, FlintType*);
bool krui_isConnected(int);
FlintType krui_getLinkWeight();
void krui_setLinkWeight(FlintTypeParam);
krui_err krui_createLink(int, FlintTypeParam);
krui_err krui_deleteLink();
krui_err krui_deleteAllInputLinks();
krui_err krui_deleteAllOutputLinks();
void krui_jogWeights(FlintTypeParam, FlintTypeParam);
int krui_countLinks();


int sci_ann_get_first_pred_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFirstPredUnit(weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(3, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_get_next_pred_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNextPredUnit(weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(3, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_get_current_pred_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getCurrentPredUnit(weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(3, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_get_first_succ_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_source_unit_id;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_source_unit_id = 1, col_source_unit_id = 1;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_source_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int source_unit_id;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_source_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id, &var_source_unit_id);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Parameter copy ***/
    source_unit_id = *var_source_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFirstSuccUnit(source_unit_id, weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(4, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;
    LhsVar(2) = 4;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_get_next_succ_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNextSuccUnit(weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(3, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_are_connected(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_source_unit_id;
    int *var_target_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_source_unit_id = 1, col_source_unit_id = 1;
    int row_target_unit_id = 1, col_target_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_source_unit_id = NULL;
    int *ptr_target_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int source_unit_id;
    int target_unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_source_unit_id);
    getVarAddressFromPosition(3, &ptr_target_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id);
    getVarDimension(ptr_target_unit_id, &row_target_unit_id, &col_target_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id, &var_source_unit_id);
    getMatrixFromDoubleTo_int(ptr_target_unit_id, &row_target_unit_id, &col_target_unit_id, &var_target_unit_id);

    /*** Parameter copy ***/
    source_unit_id = *var_source_unit_id;
    target_unit_id = *var_target_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_areConnected(source_unit_id, target_unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;


    return 0;
}


int sci_ann_are_connected_weight(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_source_unit_id;
    int *var_target_unit_id;
    int *var_sci_ret;
    double *var_weight_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_source_unit_id = 1, col_source_unit_id = 1;
    int row_target_unit_id = 1, col_target_unit_id = 1;
    int row_weight = 1, col_weight = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_weight_ret = 1, col_weight_ret = 1;

    /*** Address parameters ***/
    int *ptr_source_unit_id = NULL;
    int *ptr_target_unit_id = NULL;
    int *ptr_sci_ret = NULL;
    int *ptr_weight_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int source_unit_id;
    int target_unit_id;
    double* weight;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_source_unit_id);
    getVarAddressFromPosition(3, &ptr_target_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id);
    getVarDimension(ptr_target_unit_id, &row_target_unit_id, &col_target_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id, &var_source_unit_id);
    getMatrixFromDoubleTo_int(ptr_target_unit_id, &row_target_unit_id, &col_target_unit_id, &var_target_unit_id);

    /*** Memory allocation ***/
    weight = (double*) malloc(row_weight * col_weight * sizeof(double));

    /*** Parameter copy ***/
    source_unit_id = *var_source_unit_id;
    target_unit_id = *var_target_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_areConnectedWeight(source_unit_id, target_unit_id, weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDouble(5, row_weight_ret, col_weight_ret, weight);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;
    LhsVar(2) = 5;

    /*** Memory free ***/
    free(weight);


    return 0;
}


int sci_ann_is_connected(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_source_unit_id;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_source_unit_id = 1, col_source_unit_id = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_source_unit_id = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int source_unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_source_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id, &var_source_unit_id);

    /*** Parameter copy ***/
    source_unit_id = *var_source_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_isConnected(source_unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(3, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;


    return 0;
}


int sci_ann_get_link_weight(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    double sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getLinkWeight();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDouble(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_link_weight(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_weight;

    /*** Dimension parameters ***/
    int res_network;
    int row_weight = 1, col_weight = 1;

    /*** Address parameters ***/
    int *ptr_weight = NULL;

    /*** Source parameters ***/
    double weight;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_weight);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_weight, &row_weight, &col_weight);

    /*** Getting parameters from stack ***/
    getMatrixOfDouble(ptr_weight, &row_weight, &col_weight, &var_weight);

    /*** Parameter copy ***/
    weight = *var_weight;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setLinkWeight(weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_create_link(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_source_unit_id;
    double *var_weight;

    /*** Dimension parameters ***/
    int res_network;
    int row_source_unit_id = 1, col_source_unit_id = 1;
    int row_weight = 1, col_weight = 1;

    /*** Address parameters ***/
    int *ptr_source_unit_id = NULL;
    int *ptr_weight = NULL;

    /*** Source parameters ***/
    int sci;
    int source_unit_id;
    double weight;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_source_unit_id);
    getVarAddressFromPosition(3, &ptr_weight);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id);
    getVarDimension(ptr_weight, &row_weight, &col_weight);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_source_unit_id, &row_source_unit_id, &col_source_unit_id, &var_source_unit_id);
    getMatrixOfDouble(ptr_weight, &row_weight, &col_weight, &var_weight);

    /*** Parameter copy ***/
    source_unit_id = *var_source_unit_id;
    weight = *var_weight;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_createLink(source_unit_id, weight);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_delete_link(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteLink();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_delete_all_input_links(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteAllInputLinks();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_delete_all_output_links(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_deleteAllOutputLinks();

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_jog_weights(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_minus;
    double *var_plus;

    /*** Dimension parameters ***/
    int res_network;
    int row_minus = 1, col_minus = 1;
    int row_plus = 1, col_plus = 1;

    /*** Address parameters ***/
    int *ptr_minus = NULL;
    int *ptr_plus = NULL;

    /*** Source parameters ***/
    double minus;
    double plus;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_minus);
    getVarAddressFromPosition(3, &ptr_plus);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_minus, &row_minus, &col_minus);
    getVarDimension(ptr_plus, &row_plus, &col_plus);

    /*** Getting parameters from stack ***/
    getMatrixOfDouble(ptr_minus, &row_minus, &col_minus, &var_minus);
    getMatrixOfDouble(ptr_plus, &row_plus, &col_plus, &var_plus);

    /*** Parameter copy ***/
    minus = *var_minus;
    plus = *var_plus;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_jogWeights(minus, plus);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_count_links(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_countLinks();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


