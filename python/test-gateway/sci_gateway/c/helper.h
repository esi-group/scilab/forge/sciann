#include <stdio.h>
#include <math.h>
#include <string.h>

#include "stack-c.h"
#include "Scierror.h"

#include "kr_typ.h"
#include "kr_mem.h"

#include "glob_typ.h"
#include "kr_ui.h"

#include "enzo_mem_typ.h"

#define POS_LABELS                  1
#define POS_STRUCT_POINTER          2
#define POS_STRUCTTRAINDATA_POINTER POS_STRUCT_POINTER
#define POS_STRUCTERROR_POINTER     POS_STRUCT_POINTER
#define POS_STRUCTERROR_FILE        3

int ctos_memNet(memNet *ann,unsigned int StackPos);
memNet * stoc_memNet(unsigned int StackPos, int * res);
