#include <mex.h> 
#include <sci_gateway.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc sci_snnsversion;
extern Gatefunc sci_loadnetwork;
extern Gatefunc sci_runnetwork;
extern Gatefunc sci_ann_get_version;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,sci_snnsversion,"snns_version"},
  {(Myinterfun)sci_gateway,sci_loadnetwork,"snns_loadnetwork"},
  {(Myinterfun)sci_gateway,sci_runnetwork,"snns_runnetwork"},
  {(Myinterfun)sci_gateway,sci_ann_get_version,"ann_version"},
};
 
int C2F(libsnns_c)()
{
  Rhs = Max(0, Rhs);
  (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  return 0;
}
