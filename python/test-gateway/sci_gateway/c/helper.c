#include "helper.h"

//--------------------------------------------------------------------------------------------------------
int ctos_memNet(memNet *ann,unsigned int StackPos)
{
  int m_list_labels,  n_list_labels;
  int m_struct_pointer, n_struct_pointer;
  int m_extra,        n_extra,   l_extra; //The struct field names
  static char *fieldnames[] = {"snnslist","snns"};

  m_struct_pointer = 1; n_struct_pointer = 1;
  m_extra        = 2;   n_extra        = 1;

  CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
  m_list_labels = 2; n_list_labels = 1;
  CreateListVarFromPtr(StackPos, POS_LABELS,       "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
  CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_struct_pointer, &n_struct_pointer, ann);

  return 0;
}





//--------------------------------------------------------------------------------------------------------
memNet * stoc_memNet(unsigned int StackPos, int * res)
{
  int m_struct_pointer, n_struct_pointer, l_struct_pointer;
  int m_param,        n_param,        l_param;
  int m_label,        n_label;
  char ** LabelList;
  memNet * result_ann = NULL;

  GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
  GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

  if (strcmp(LabelList[0],"snnslist") != 0) 
    {
      Scierror(999,"Argument 1 is not a snnslist\r\n");
    }

  GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_struct_pointer,  &n_struct_pointer,  &l_struct_pointer);

  result_ann = (memNet *)((unsigned long)*stk(l_struct_pointer));

  *res = 0;

  return result_ann;
}

