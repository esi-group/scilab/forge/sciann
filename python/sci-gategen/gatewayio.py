"""
Gateway I/O module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.  ##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/

import sys
import os
import logging

import gateway
import ut

from gateway import GatewayParameter
from gateway import GatewayPrototype
from gateway import GatewayParameterDimension

logger = logging.getLogger(os.path.basename(sys.argv[0]))


prefix_row = 'row_'
prefix_column = 'col_'
prefix_address = 'ptr_'
prefix_parameter = 'var_'
prefix_length = 'lengths_'
prefix_strings = 'strings_'
prefix_counter = 'cnt_'
prefix_result = 'res_'
suffix_return = '_ret'

# Module methods
# TODO: this is related to the C language and shouldn't be here!

def is_string(gw_parameter):
    type = gw_parameter.name_type.replace('*', '')
    if type == 'char' and gw_parameter.nb_pointers > 0 and isinstance(gw_parameter, GatewayParameter):
        return True
    else:
        return False


def get_stack_functions(gw_parameter, tokenizer):
    names_type_to_code = {'double': ('getMatrixOfDouble', 'createMatrixOfDouble')}

    # replace the st method by a call to a tokenzier
    if is_string(gw_parameter):
        ret = ('getMatrixOfString', 'createMatrixOfString')
    elif tokenizer.is_conversion_to_double_needed(gw_parameter.name_type):
        name_type = tokenizer.get_type_in_string(gw_parameter.name_type)
        name_type_escaped = name_type.replace(' ', '_')
        ret = ('getMatrixFromDoubleTo_%s' % name_type_escaped, 'createMatrixOfDoubleFrom_%s' % name_type_escaped) 
    else:
        ret = names_type_to_code.get(gw_parameter.name_type.replace('*', ''), ('UNKNOWN', 'UNKNOWN'))

    return ret

# TODO delete if non used
def search_writer_parameter(writers_parameter, gw_parameter):
    for writer_parameter in writers_parameter:
        if writer_parameter.gw_parameter == gw_parameter:
            return writer_parameter
    return None



class GatewayIO:
    pass



class GatewayIO_Scilab52(GatewayIO):

    name_file_helper_body_output = 'helper_%s.c'
    name_file_helper_body_template = 'templates/helper_%s.c'

    name_file_helper_header_output = 'helper_%s.h'
    name_file_helper_header_template = 'templates/helper_%s.h'

    name_file_template_doc_body = 'templates/doc_body.xml'
    name_file_template_doc_parameter = 'templates/doc_parameter.xml'
    name_file_template_doc_parameters = 'templates/doc_parameters.xml'
    name_file_template_doc_seealso = 'templates/doc_seealso.xml'
    name_file_template_doc_author = 'templates/doc_author.xml'

    # TODO replace with a call to the language tokenizer 
    name_file_builder = 'builder_gateway_%s.sce'
    name_file_template_builder_body = 'templates/builder_gateway_body.sce'
    name_file_template_builder_function = 'templates/builder_gateway_function.sce'
    name_file_template_builder_file = 'templates/builder_gateway_file.sce'


    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.path_directory_output = './'

    
    def init_output_directory(self, path_directory_output):
        self.path_directory_output = path_directory_output
        if not os.path.exists(self.path_directory_output):
            os.makedirs(self.path_directory_output)


    def read_template(self, name_file):
        # TODO handle file exceptions
        file_template = open(name_file, 'r')
        template = file_template.read()
        file_template.close()
        return template
        

    def write_helper(self, name_helper, names_type, message_logger, headers=None):
        # TODO handle file exceptions

        # Write the files only if types have to be translated
        if not names_type:
            return

        template_helper_body = self.read_template(self.name_file_helper_body_template % name_helper)
        template_helper_header = self.read_template(self.name_file_helper_header_template % name_helper)

        file_helper_body = open(os.path.join(self.path_directory_output, self.name_file_helper_body_output % name_helper), 'w') 
        file_helper_header = open(os.path.join(self.path_directory_output, self.name_file_helper_header_output % name_helper), 'w') 

        for index, name_type in enumerate(names_type):
            logger.info('%s: "%s"' % (message_logger, name_type))
            words = name_type.split(' ')
            name_type_escaped = '_'.join(words)

            name_header = self.name_file_helper_header_output % name_helper
            body_top = '' if index > 0 else '#include "%s"' % name_header

            helper_body = template_helper_body % {'name_type': name_type, 'name_type_escaped': name_type_escaped, 'top': body_top}
            file_helper_body.write(helper_body)

            header_top = '' if index > 0 else '\n'.join(headers)
            helper_header = template_helper_header % {'name_type': name_type, 'name_type_escaped': name_type_escaped, 'top': header_top}
            file_helper_header.write(helper_header)

        file_helper_body.close()
        file_helper_header.close()
            

    def write_function_call(self, file, gw_prototype):
        file.write('\n    // *** Function call\n')
        function_call = self.tokenizer.string_function_call(gw_prototype)
        #print 'call:', function_call
        file.write('    ' + function_call + '\n\n')


    def indent_string(self, size=4):
        def indent(string):
            return ' ' * size + string + '\n'
        return indent


    def write_gw_operations(self, file, sts, comment):
        indent = self.indent_string(4)
        if sts and [s for s in sts if s != None]: 
            #file.write(' ' * size_indent + '/*** ' + comment + ' ***/\n')
            file.write(indent('/*** ' + comment + ' ***/'))
            # TODO use list comprehension
            #for st in sts:
            #    if st:
            #        for s in ut.make_list(st):
            #            file.write(indent(s))
            #file.write('\n')
            #file.write(''.join([indent(s) for s in ut.splat([ut.make_list(st) for st in sts if st])]) + '\n')
            file.write(''.join([indent(s) for st in sts if st for s in ut.make_list(st)]) + '\n')


    def parameter_gw_to_wr(self, gw_prototype, gw_parameter):
        wr_parameter = None

        if is_string(gw_parameter):
            wr_parameter = WriterParameterString(gw_parameter, gw_prototype, self.tokenizer) 
        elif not self.tokenizer.is_name_type_valid(gw_parameter.name_type):

            wr_parameter = WriterParameterUserType(gw_parameter, gw_prototype, self.tokenizer)
        elif gw_parameter.nb_pointers == 1: 
            wr_parameter = WriterParameterVector(gw_parameter, gw_prototype, self.tokenizer) 
        elif gw_parameter.nb_pointers == 2: 
            wr_parameter = WriterParameterMatrix(gw_parameter, gw_prototype, self.tokenizer) 
        else:
            wr_parameter = WriterParameter(gw_parameter, gw_prototype, self.tokenizer) 
        return wr_parameter

    # TODO rename these functions: switch signature and gw_prototype
    def get_gw_prototype_signature(self, gw_prototype):
        parameter_return = gw_prototype.get_parameter_return_source()
        type_parameter_return = '%s' % parameter_return.name_type_original if parameter_return else 'void'
        types_parameter = [p.name_type_original for p in gw_prototype.get_parameters_source()]
        return '%s %s(%s);' % (type_parameter_return, gw_prototype.name_function, ', '.join(types_parameter))


    def write_gw_prototype_signature(self, file, gw_prototype):
        file.write(self.get_gw_prototype_signature(gw_prototype) + '\n')


    def write_parameter_check(self, name_function, number):
        number_min = number
        number_max = number if number else 1
        return '%(name_function)s(%(number_min)s, %(number_max)s);' % {'name_function': name_function, 'number_min': number_min, 'number_max': number_max}
        

    def write_gw_prototype(self, file, gw_prototype):
        # Header
        header = 'int %(name_gateway)s(char *fname)\n{\n' % {'name_gateway': gw_prototype.name_gateway}
        file.write(header)


        # Make the ParameterWriter sequence 
        wr_parameters = [self.parameter_gw_to_wr(gw_prototype, gw_parameter) for gw_parameter in gw_prototype.get_parameters()]

        # Definitions of the destination parameters
        sts_definition_destination = [p.definition_destination() for p in wr_parameters if p.is_destination()]
        sts_definition_destination.extend([p.definition_destination(suffix_return) for p in wr_parameters if p.is_return()])
        self.write_gw_operations(file, sts_definition_destination, 'Destination parameters')

        # Definitions of the dimension parameters
        sts_definition_dimensions = [p.definition_dimension() for p in wr_parameters if (p.is_source() or p.is_destination() or p.is_dimension_destination()) and not p.is_source_return()]
        sts_definition_dimensions.extend([p.definition_dimension(suffix_return) for p in wr_parameters if p.is_return() or p.is_dimension_return()])
        self.write_gw_operations(file, sts_definition_dimensions, 'Dimension parameters')

        # Definitions of the address parameters
        sts_definition_dimensions = [p.definition_address() for p in wr_parameters if p.is_destination()]
        sts_definition_dimensions.extend([p.definition_address(suffix_return) for p in wr_parameters if p.is_return()])
        self.write_gw_operations(file, sts_definition_dimensions, 'Address parameters')

        # Definitions of the source parameters
        sts_definition_source = [p.definition_source() for p in wr_parameters if p.is_source() or p.is_source_return()]
        self.write_gw_operations(file, sts_definition_source, 'Source parameters')

        # Check number of parameters
        sts_check_output = self.write_parameter_check('CheckRhs', len(gw_prototype.get_parameters_destination()))
        sts_check_input = self.write_parameter_check('CheckLhs', len(gw_prototype.get_parameters_return()))
        self.write_gw_operations(file, [sts_check_output, sts_check_input], 'Check for number of parameters')

        # Getting the address of the parameters
        sts_address = [p.address(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_destination()])]
        self.write_gw_operations(file, sts_address, 'Getting the address of the parameters')

        # Getting the dimensions of the parameters
        sts_dimension = [p.dimension(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_destination()])]
        self.write_gw_operations(file, sts_dimension, 'Getting the dimensions of the parameters') 

        # Prerequisites destination
        sts_prerequisite_dest = [p.prerequisite_dest(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_destination()])]
        self.write_gw_operations(file, sts_prerequisite_dest, 'Prerequisites destination')

        # Getting parameter from stack
        sts_stack_read = [p.stack_read(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_destination()])]
        self.write_gw_operations(file, sts_stack_read, 'Getting parameters from stack')

        # Memory allocation - and not dest because in that case, the allocation is made by scilab
        sts_memory_allocation = [p.memory_allocation() for p in wr_parameters if p.is_source() and not p.is_destination() and not p.is_source_return()]
        self.write_gw_operations(file, sts_memory_allocation, 'Memory allocation')

        # Passed parameters copy
        sts_parameter_copy = [p.parameter_copy() for p in wr_parameters if (p.is_destination() or p.is_dimension_destination()) and p.is_source()]
        self.write_gw_operations(file, sts_parameter_copy, 'Parameter copy')


        # Hook function start
        self.write_gw_operations(file, gw_prototype.hooks_start, 'Hook functions start')

        # function call
        self.write_function_call(file, gw_prototype)
        #self.write_gw_operations(file, [self.tokenizer.function_call(gw_prototype)], 'Function call')

        # Hook function end
        self.write_gw_operations(file, gw_prototype.hooks_end, 'Hook functions end')

        # String check
        sts_check_string = [p.check_string(index + 1) for index, p in enumerate([p for p in wr_parameters if isinstance(p, WriterParameterString) and p.is_return()])]
        self.write_gw_operations(file, sts_check_string, 'String check')

        # Prerequisites return
        sts_prerequisite_return = [p.prerequisite_return(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_return()])]
        self.write_gw_operations(file, sts_prerequisite_return, 'Prerequisites return')

        # Returned parameters copy
        sts_return_copy = [p.return_copy() for p in [p for p in wr_parameters if p.is_source() and (p.is_return() or p.is_dimension_return())]]
        #sts_return_copy = [p.return_copy() for p in [p for p in wr_parameters if p.is_dimension_return()]]
        self.write_gw_operations(file, sts_return_copy, 'Copy return parameters')

        # Create a sublist for the next st writings
        wr_parameters_return = [p for p in wr_parameters if p.is_return()]

        # Create return parameter
        offset_stack = len(sts_stack_read)
        sts_stack_write = [p.stack_write(index + offset_stack + 1) for index, p in enumerate(wr_parameters_return)]
        self.write_gw_operations(file, sts_stack_write, 'Create return parameters')

        # Set the returned parameters on the stack
        sts_stack_return = [p.stack_return(index + offset_stack + 1, index + 1) for index, p in enumerate(wr_parameters_return)]
        self.write_gw_operations(file, sts_stack_return, 'Set return parameters on stack')

        # Postrequisites destination
        sts_postrequisite_dest = [p.postrequisite_dest(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_destination()])]
        self.write_gw_operations(file, sts_postrequisite_dest, 'Postrequisites destination')

        # Postrequisites return
        sts_postrequisite_return = [p.postrequisite_return(index + 1) for index, p in enumerate([p for p in wr_parameters if p.is_return()])]
        self.write_gw_operations(file, sts_postrequisite_return, 'Postrequisites return')

        # Memory free
        sts_memory_free = [p.memory_free() for p in wr_parameters if p.is_source() and not p.is_destination() and not p.is_source_return()]
        self.write_gw_operations(file, sts_memory_free, 'Memory free')

        # TODO: what I want using closure or decorators is a one line statement:

        #self.do_stuff('memory_free', 

        # Footer
        footer = '\n    return 0;\n}\n'
        file.write(footer)
        file.write('\n\n')


    def write_gw_files(self, gw_files, headers_additional):
        headers_common = '\n'.join([h for h in headers_additional if h])
        for gw_file in gw_files.values():
            file_gateway = None
            try:
                logger.debug('Write file: "%s"' % gw_file.name_file)
                #print 'write file:', gw_file.name_file
                if gw_file.gw_prototypes:
                    file_gateway = open(os.path.join(self.path_directory_output, gw_file.name_file), 'w')

                    file_gateway.write('#include "stack-c.h"\n')
                    file_gateway.write('#include "Scierror.h"\n')
                    file_gateway.write('#include "localization.h"\n')
                    file_gateway.write('#include "sciprint.h"\n')
                    #file_gateway.write('#include "api_variable.h"\n\n')

                    if headers_common:
                        file_gateway.write(headers_common + '\n\n')

                    if gw_file.headers:
                        headers_file = '\n'.join([h for h in gw_file.headers])
                        file_gateway.write(headers_file + '\n\n\n')

                    for gw_prototype in gw_file.gw_prototypes:
                        logger.debug('Write prototype: "%s"' % gw_prototype.name_function)
                        self.write_gw_prototype_signature(file_gateway, gw_prototype)

                    file_gateway.write('\n\n')

                    for gw_prototype in gw_file.gw_prototypes: 
                        self.write_gw_prototype(file_gateway, gw_prototype)

            except IOError, e:
                raise IOError, e
            finally:
                if file_gateway:
                    file_gateway.close()
            pass


    def build_seealso(self, template_doc_seealso):
        return '<member><link linkend="">name</link></member>'


    def build_authors(self, template_doc_author, authors):
        if not authors:
            authors = 'NULL'
        return ''.join([template_doc_author % {'author': a} for a in authors])


    def build_descriptions_parameter(self, gw_parameters, title):
        template_doc_parameters = self.read_template(self.name_file_template_doc_parameters)
        template_doc_parameter = self.read_template(self.name_file_template_doc_parameter)
        if gw_parameters:
            description = [template_doc_parameter % {'name_parameter': p.name_parameter_new} for p in gw_parameters]
            descriptions = template_doc_parameters % {'title_parameter': title, 'description_parameter': ''.join(description)}
        else:
            descriptions = ''
        return descriptions


    def write_documentation_prototype(self, gw_prototype, name_dir, authors):
        descriptions_output = self.build_descriptions_parameter(gw_prototype.get_parameters_return(), 'Output parameters')
        list_output = ', '.join([p.name_parameter_new for p in gw_prototype.get_parameters_return()])
        if list_output: list_output = '[' + list_output + '] = '

        descriptions_input = self.build_descriptions_parameter(gw_prototype.get_parameters_destination(), 'Input parameters')
        list_input = ', '.join([p.name_parameter_new for p in gw_prototype.get_parameters_destination()])

        template_doc_seealso = self.read_template(self.name_file_template_doc_seealso)
        seealso = self.build_seealso(template_doc_seealso)

        template_doc_author = self.read_template(self.name_file_template_doc_author)
        list_authors = self.build_authors(template_doc_author, authors)

        template_doc_body = self.read_template(self.name_file_template_doc_body)
        name_file_doc = os.path.join(name_dir, gw_prototype.name_function_new + '.xml')

        descriptions_parameter = descriptions_input + descriptions_output

        file_doc = open(os.path.join(self.path_directory_output, name_file_doc), 'w')
        doc = template_doc_body % {'id': gw_prototype.name_function, \
                                   'name_function': gw_prototype.name_function, \
                                   'name_function_new': gw_prototype.name_function_new, \
                                   'authors': list_authors, \
                                   'list_output': list_output, \
                                   'list_input': list_input, \
                                   'descriptions_parameter': descriptions_parameter, \
                                   'seealso': seealso, \
                                   'comments': self.get_gw_prototype_signature(gw_prototype) + '\n' + gw_prototype.comments }
        file_doc.write(doc)
        file_doc.close()
        logger.debug('Writing help file: %s%s(%s)' % (list_output, gw_prototype.name_function_new, list_input))
        
    # TODO: to be clean, the template files should be opened only one time here
    # and not in the sub function when writing every single help file.
    def write_documentation_prototypes(self, gw_prototypes, name_dir, authors):
        # TODO catch exception
        path_dir = os.path.join(self.path_directory_output, name_dir)
        if not os.path.exists(path_dir):
            os.makedirs(path_dir)

        for gw_prototype in gw_prototypes: 
            self.write_documentation_prototype(gw_prototype, name_dir, authors)
         

    def write_builder(self, gw_files, gw_prototypes, name_project, name_language, names_file_additional, libraries, links, includes):
        name_file_builder = self.name_file_builder % name_language
        path_call = 'get_absolute_file_path(\'%s\')' % name_file_builder
        path_string = '\' + %s + \'' % path_call

        template_builder_function = self.read_template(self.name_file_template_builder_function)
        template_builder_file = self.read_template(self.name_file_template_builder_file)

        sts_file = [template_builder_file % {'name_file': f.name_file} for f in gw_files.values() if f.gw_prototypes]

        sts_file_additional = [template_builder_file % {'name_file': name_file} for name_file in names_file_additional if name_file]

        sts_gateway = [template_builder_function % {'name_function': p.name_function_new, 'name_gateway': p.name_gateway} for p in gw_prototypes if not p.error_type]

        template_builder_body = self.read_template(self.name_file_template_builder_body)
        libraries = ['\'' + name + '\'' for name in libraries]
        fields_body = {'name_toolbox': name_project, \
                       'path': path_call, \
                       'list_gateways': ''.join(sts_gateway), \
                       'list_files': ''.join(sts_file) + ''.join(sts_file_additional), \
                       'libraries': ', '.join(libraries).replace('PATH', path_string), \
                       'links': ' '.join(links), \
                       'includes': ' '.join(includes).replace('PATH', path_string) }
        builder = template_builder_body % fields_body

        file_builder = open(os.path.join(self.path_directory_output, name_file_builder), 'w')
        file_builder.write(builder)
        file_builder.close()


class WriterParameter:
    """
    Encapsulates all the information related to the passing of a particular
    parameter, that is to say what to write in the gateway at each step
    of the use of the parameter.
    """


    def __init__(self, gw_parameter, gw_prototype, tokenizer):
        self.gw_parameter = gw_parameter
        self.gw_prototype = gw_prototype
        self.tokenizer = tokenizer

        (name_function_get, name_function_set) = get_stack_functions(self.gw_parameter, tokenizer)
        self.fields = {'prefix_row': prefix_row, \
                       'prefix_column': prefix_column, \
                       'prefix_parameter': prefix_parameter, \
                       'prefix_address': prefix_address, \
                       'prefix_length': prefix_length, \
                       'prefix_strings': prefix_strings, \
                       'prefix_counter': prefix_counter, \
                       'prefix_result': prefix_result, \
                       'suffix_return': suffix_return, \
                       'parameter': self.gw_parameter.name_parameter_new, \
                       'type': self.gw_parameter.name_type, \
                       'type_primitive': self.tokenizer.get_type_in_string(self.gw_parameter.name_type), \
                       'type_minus': self.tokenizer.get_type_in_string(self.gw_parameter.name_type) + '*' * (self.gw_parameter.nb_pointers - 1), \
                       'prefix_definition': prefix_parameter, \
                       'suffix_definition': '', \
                       'name_function_get': name_function_get, \
                       'name_function_set': name_function_set, \
                       'name_function_dimension': 'getVarDimension', \
                       'name_function_address': 'getVarAddressFromPosition', \
                       'dereferencing': '*' * self.gw_parameter.nb_pointers, \
                       'current_to_pointer': '&', \
                       'pointer_to_current': '*' \
                      }

        if not isinstance(self.gw_parameter, GatewayParameterDimension):
            self.is_dimension = False
        else:
            self.is_dimension = True
            #print 'dimension:', self.gw_parameter.name_parameter_new
            gw_parameter_matrix = self.gw_parameter.parameter_matrix
            self.fields['matrix'] = gw_parameter_matrix.name_parameter_new
            self.fields['prefix_dimension'] = self.fields['prefix_row'] if gw_parameter_matrix.parameter_dimension_row != None else self.fields['prefix_column']


    # TODO refactor all the uses of these four functions, and use direclty
    # the methods from the module gateway.py
    # Tests on the parameter, the test against None is important
    def is_source(self):
        return True if self.gw_parameter.position_source != None else False

    def is_source_return(self):
        return True if self.gw_parameter.position_source == -1 else False

    def is_destination(self):
        return True if self.gw_parameter.position_destination != None else False

    def is_return(self):
        return True if self.gw_parameter.position_return != None else False

    def is_dimension_destination(self):
        return True if self.is_dimension and gateway.is_destination(self.gw_parameter.parameter_matrix) else False

    def is_dimension_return(self):
        return True if self.is_dimension and gateway.is_return(self.gw_parameter.parameter_matrix) else False

    # String builders
    def definition_source(self):
        #return self.tokenizer.parameter_definition(self.gw_parameter)
        return '%(type)s %(parameter)s;' % self.fields


    def definition_destination(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        return '%(type_primitive)s *%(prefix_definition)s%(parameter)s%(suffix_definition)s;' % self.fields


    def definition_dimension(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        return 'int %(prefix_row)s%(parameter)s%(suffix_definition)s = 1, %(prefix_column)s%(parameter)s%(suffix_definition)s = 1;' % self.fields


    def definition_address(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        return 'int *%(prefix_address)s%(parameter)s%(suffix_definition)s = NULL;' % self.fields


    def address(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return '%(name_function_address)s(%(index_stack)s, &%(prefix_address)s%(parameter)s);' % self.fields


    def dimension(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return '%(name_function_dimension)s(%(prefix_address)s%(parameter)s, &%(prefix_row)s%(parameter)s, &%(prefix_column)s%(parameter)s);' % self.fields


    def prerequisite_dest(self, index_stack=None):
        pass


    def stack_read(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return '%(name_function_get)s(%(prefix_address)s%(parameter)s, &%(prefix_row)s%(parameter)s, &%(prefix_column)s%(parameter)s, &%(prefix_parameter)s%(parameter)s);' % self.fields
            #template = '%(parameter)s = %(pointer_to_current)s%(prefix_parameter)s%(parameter)s;' if self.is_destination() else '%(parameter)s = 0;'


    def memory_allocation(self):
        pass


    def memory_free(self):
        pass


    def parameter_copy(self):
        # REMEMBER: the sts used to start with "%(dereferencing)s"
        if self.is_dimension_destination():
            template = '%(parameter)s = %(prefix_dimension)s%(matrix)s;' 
        else:
            #return None
            template = '%(parameter)s = %(pointer_to_current)s%(prefix_parameter)s%(parameter)s;'# if self.is_destination() else '%(parameter)s = 0;'
        return template % self.fields


    def stack_write(self, index_stack):
        self.fields['index_stack'] = index_stack
        #return '%(name_function_set)s(%(index_stack)s, %(prefix_row)s%(parameter)s%(suffix_return)s, %(prefix_column)s%(parameter)s%(suffix_return)s, %(prefix_parameter)s%(parameter)s%(suffix_return)s);' % self.fields
        return '%(name_function_set)s(%(index_stack)s, %(prefix_row)s%(parameter)s%(suffix_return)s, %(prefix_column)s%(parameter)s%(suffix_return)s, %(current_to_pointer)s%(parameter)s);' % self.fields
        #template = '%(prefix_parameter)s%(parameter)s%(suffix_return)s = %(current_to_pointer)s%(parameter)s;'

    def return_copy(self):
        if self.is_dimension_return():
            template = '%(prefix_dimension)s%(matrix)s%(suffix_return)s = %(dereferencing)s%(parameter)s;'
        else:
            return None 
            #template = '%(prefix_parameter)s%(parameter)s%(suffix_return)s = %(current_to_pointer)s%(parameter)s;'
        return template % self.fields


    def postrequisite_dest(self, index_stack=None):
        pass

    def prerequisite_dest(self, index_stack=None):
        pass

    def prerequisite_return(self, index_stack=None):
        pass

    def postrequisite_return(self, index_stack=None):
        pass


    def stack_return(self, index_stack, index_return):
        return 'LhsVar(%(index_return)d) = %(index_stack)d;' % {'index_stack': index_stack, 'index_return': index_return}



class WriterParameterVector(WriterParameter):
    """
    Handles vectors, that is to say one-dimension arrays.
    """

    def __init__(self, gw_parameter, gw_prototype, tokenizer):
        WriterParameter.__init__(self, gw_parameter, gw_prototype, tokenizer)
        self.fields['current_to_pointer'] = ''
        self.fields['pointer_to_current'] = ''


    def memory_allocation(self):
        return '%(parameter)s = (%(type)s) malloc(%(prefix_row)s%(parameter)s * %(prefix_column)s%(parameter)s * sizeof(%(type_minus)s));' % self.fields


    def memory_free(self):
        return 'free(%(parameter)s);' % self.fields



class WriterParameterString(WriterParameter):
    """
    Handles strings.
    """

    def __init__(self, gw_parameter, gw_prototype, tokenizer):
        WriterParameter.__init__(self, gw_parameter, gw_prototype, tokenizer)
        if gw_parameter.nb_pointers == 1:
            self.fields['current_to_pointer'] = ''
            self.fields['pointer_to_current'] = ''
        elif gw_parameter.nb_pointers == 2:
            self.fields['current_to_pointer'] = '*'
            self.fields['pointer_to_current'] = '&'


    def memory_allocation(self):
        if self.gw_parameter.nb_pointers == 2:
            return '%(parameter)s = (%(type)s) malloc(sizeof(%(type_minus)s));' % self.fields
        else:
            return None


    def memory_free(self):
        if self.gw_parameter.nb_pointers == 2:
            return 'free(%(parameter)s);' % self.fields
        else:
            return None


    def definition_address(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        addresses = ut.make_list(WriterParameter.definition_address(self, suffix_definition))
        addresses.extend(['int %(prefix_counter)s%(parameter)s%(suffix_definition)s;' % self.fields, 'int *%(prefix_length)s%(parameter)s%(suffix_definition)s = NULL;' % self.fields, 'char **%(prefix_strings)s%(parameter)s%(suffix_definition)s = NULL;' % self.fields])
        return addresses


    def definition_dimension(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        return 'int %(prefix_row)s%(parameter)s%(suffix_definition)s = 1, %(prefix_column)s%(parameter)s%(suffix_definition)s = 1;' % self.fields


    def parameter_copy(self):
        if self.is_destination():
            if self.gw_parameter.nb_pointers == 1: 
                template = '%(parameter)s = %(prefix_strings)s%(parameter)s[0];'
            else:
                template = '%(parameter)s = %(prefix_strings)s%(parameter)s;'
        else:
            return None
        return template % self.fields


    def return_copy(self):
        # TODO introduce a new replacement called 'current_to_matrix', and rename 'current_to_pointer' for 'current_to_vector'
        #if self.gw_parameter.nb_pointers == 1: 
            # this is unlikely to work, maybe it will require a malloc first
        #    template = '%(prefix_strings)s%(parameter)s%(suffix_return)s = &%(parameter)s;'
        #else:
        #    template = '%(prefix_strings)s%(parameter)s%(suffix_return)s = %(parameter)s;'
            
        #return template % self.fields
        pass


    def stack_read(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return '%(name_function_get)s(%(prefix_address)s%(parameter)s, &%(prefix_row)s%(parameter)s, &%(prefix_column)s%(parameter)s, %(prefix_length)s%(parameter)s, %(prefix_strings)s%(parameter)s);' % self.fields


    def stack_write(self, index_stack):
        self.fields['index_stack'] = index_stack
        #if not self.is_source_return():
        sts = ['%(prefix_strings)s%(parameter)s%(suffix_return)s = (char**) malloc(%(prefix_row)s%(parameter)s%(suffix_return)s * %(prefix_column)s%(parameter)s%(suffix_return)s * sizeof(char*));' % self.fields]
        if self.gw_parameter.nb_pointers == 1: 
            sts.append('%(prefix_strings)s%(parameter)s%(suffix_return)s[0] = %(parameter)s;' % self.fields)
        else:
            sts.append('for(%(prefix_counter)s%(parameter)s%(suffix_return)s = 0; %(prefix_counter)s%(parameter)s%(suffix_return)s < %(prefix_row)s%(parameter)s%(suffix_return)s * %(prefix_column)s%(parameter)s%(suffix_return)s ; ++%(prefix_counter)s%(parameter)s%(suffix_return)s) %(prefix_strings)s%(parameter)s%(suffix_return)s[%(prefix_counter)s%(parameter)s%(suffix_return)s] = %(parameter)s[%(prefix_counter)s%(parameter)s%(suffix_return)s];' % self.fields)
            
        sts.append('%(name_function_set)s(%(index_stack)s, %(prefix_row)s%(parameter)s%(suffix_return)s, %(prefix_column)s%(parameter)s%(suffix_return)s, %(prefix_strings)s%(parameter)s%(suffix_return)s);' % self.fields)
        return sts


    def prerequisite_dest(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return ['%(prefix_length)s%(parameter)s = (int*) malloc(%(prefix_row)s%(parameter)s * %(prefix_column)s%(parameter)s * sizeof(int));' % self.fields, \
                '%(name_function_get)s(%(prefix_address)s%(parameter)s, &%(prefix_row)s%(parameter)s, &%(prefix_column)s%(parameter)s, %(prefix_length)s%(parameter)s, NULL);' % self.fields, \
                '%(prefix_strings)s%(parameter)s = (char**) malloc(%(prefix_row)s%(parameter)s * %(prefix_column)s%(parameter)s * sizeof(char*));' % self.fields, \
                'for(%(prefix_counter)s%(parameter)s = 0; %(prefix_counter)s%(parameter)s < %(prefix_row)s%(parameter)s * %(prefix_column)s%(parameter)s ; ++%(prefix_counter)s%(parameter)s) %(prefix_strings)s%(parameter)s[%(prefix_counter)s%(parameter)s] = (char*) malloc((%(prefix_length)s%(parameter)s[%(prefix_counter)s%(parameter)s] + 1) * sizeof(char));' % self.fields]


    def postrequisite_dest(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return ['for(%(prefix_counter)s%(parameter)s = 0; %(prefix_counter)s%(parameter)s < %(prefix_row)s%(parameter)s * %(prefix_column)s%(parameter)s ; ++%(prefix_counter)s%(parameter)s) free(%(prefix_strings)s%(parameter)s[%(prefix_counter)s%(parameter)s]);' % self.fields, \
                'free(%(prefix_strings)s%(parameter)s);' % self.fields, \
                'free(%(prefix_length)s%(parameter)s);' % self.fields]


    def check_string(self, index_stack=None):
        if self.gw_parameter.nb_pointers == 1: 
            template = 'if(%(parameter)s == NULL) { Scierror(999, "%(parameter)s is NULL"); return -1; }'
        else:
            template = 'for(%(prefix_counter)s%(parameter)s%(suffix_return)s = 0; %(prefix_counter)s%(parameter)s%(suffix_return)s < %(prefix_row)s%(parameter)s%(suffix_return)s * %(prefix_column)s%(parameter)s%(suffix_return)s ; ++%(prefix_counter)s%(parameter)s%(suffix_return)s) if(%(parameter)s[%(prefix_counter)s%(parameter)s%(suffix_return)s] == NULL) { Scierror(999, "%(parameter)s is NULL"); return -1;}'
        return template % self.fields


    def prerequisite_return(self, index_stack=None):
        pass


    def postrequisite_return(self, index_stack=None):
        return 'free(%(prefix_strings)s%(parameter)s%(suffix_return)s);' % self.fields


class WriterParameterUserType(WriterParameter):
    """
    Handles all user types.
    """

    def __init__(self, gw_parameter, gw_prototype, tokenizer):
        WriterParameter.__init__(self, gw_parameter, gw_prototype, tokenizer)
        if gw_parameter.nb_pointers == 1:
            self.fields['current_to_pointer'] = ''
            self.fields['pointer_to_current'] = ''
        elif gw_parameter.nb_pointers == 2:
            self.fields['current_to_pointer'] = '*'
            self.fields['pointer_to_current'] = '&'

        self.fields['name_function_get'] = 'stoc_%s' % '_'.join(self.fields['type_primitive'].split(' '))
        self.fields['name_function_set'] = 'ctos_%s' % '_'.join(self.fields['type_primitive'].split(' '))


    def definition_address(self, suffix_definition=''):
        pass

    def stack_read(self, index_stack=None):
        pass

    def dimension(self, index_stack=None):
        pass


    def definition_dimension(self, suffix_definition=''):
        self.fields['suffix_definition'] = suffix_definition
        return 'int %(prefix_result)s%(parameter)s%(suffix_definition)s;' % self.fields


    def parameter_copy(self):
        if self.is_destination():
            template = '%(parameter)s = %(pointer_to_current)s%(prefix_parameter)s%(parameter)s;'
        else:
            return None
        return template % self.fields


    def address(self, index_stack=None):
        self.fields['index_stack'] = index_stack
        return '%(prefix_parameter)s%(parameter)s = %(name_function_get)s(%(index_stack)s, &%(prefix_result)s%(parameter)s);' % self.fields


    def stack_write(self, index_stack):
        self.fields['index_stack'] = index_stack
        return '%(prefix_result)s%(parameter)s%(suffix_return)s = %(name_function_set)s(%(index_stack)s, %(prefix_parameter)s%(parameter)s%(suffix_return)s);' % self.fields


    def return_copy(self):
        template = '%(prefix_parameter)s%(parameter)s%(suffix_return)s = %(current_to_pointer)s%(parameter)s;'
        return template % self.fields


    def postrequisite_dest(self, index_stack=None):
        pass
        #return 'free(%(prefix_parameter)s%(parameter)s);' % self.fields
        

class WriterParameterMatrix(WriterParameterVector):
    """
    Handles vectors, that is to say one-dimension arrays.
    """

    def __init__(self, gw_parameter, gw_prototype, tokenizer):
        WriterParameterVector.__init__(self, gw_parameter, gw_prototype, tokenizer)
        self.fields['current_to_pointer'] = '*'
        self.fields['pointer_to_current'] = '&'

