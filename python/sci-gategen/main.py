"""
Main module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/

import sys
import os
import logging
from optparse import OptionParser

from tokenizer import TokenizerCLang
from gatewayio import GatewayIO_Scilab52
from generator import Generator


logger = logging.getLogger(os.path.basename(sys.argv[0]))


def configure_logging(level=logging.DEBUG):
    #LOG_FILENAME = './log.out'
    #logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
    #console = logging.StreamHandler()
    #console.setLevel(level)
    logging.basicConfig(level=level, format='%(levelname)s: %(message)s')
    #formatter = logging.Formatter('%(name)-12s: %(levelname)-8s - %(message)s')
    #console.setFormatter(formatter)
    #logging.getLogger('').addHandler(console)
    #logging.debug('This message should go to the log file')


def set_logger(options):
    if options.verbose:
        level_logging = logging.INFO
    elif options.quiet:
        level_logging = logging.CRITICAL
    elif options.debug:
        level_logging = logging.DEBUG
    else:
        level_logging = logging.WARNING
    configure_logging(level_logging) 


def set_option_parser(parser):
    path_file_config_default = 'rules.cfg'
    parser.add_option('-c', '--config', action='store', type='string', dest='path_file_config', default=path_file_config_default, help='Configuration file, where the rules will be read')
    path_directory_output_default = 'gateway'
    parser.add_option('-o', '--output', action='store', type='string', dest='path_directory_output', default=path_directory_output_default, help='Output directory, where the output files will be written')
    parser.add_option('-q', '--quiet', action='store_true', dest='quiet', default=False, help='show only critical messages')
    parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='show all messages')
    parser.add_option('-d', '--debug', action='store_true', dest='debug', default=False, help='show ALL messages')


def check_options(options):
    if not os.path.exists(options.path_file_config):
        logger.critical('Configuration file "%s" cannot be found.' % options.path_file_config)
        exit()


if __name__ == '__main__':

    parser = OptionParser(version="sci-gategen 0.1")
    set_option_parser(parser)
    (options, args) = parser.parse_args()

    set_logger(options)
    check_options(options)

    tokenizer = TokenizerCLang()
    gw_writer = GatewayIO_Scilab52(tokenizer)
    gen = Generator(tokenizer, gw_writer)
    gen.generate(options.path_file_config, options.path_directory_output)
