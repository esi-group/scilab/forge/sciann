"""
Utility module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/


def flatten(obj):
    """Return all the objects contained in an iterable object."""
    if issequence(obj):
        # TODO check if we can loop on sets
        for o in obj:
            for o_flat in flatten(o):
                yield o_flat
    else:
        yield obj
   

def issequence(obj):
    # TODO check on what is a sequence
    return isinstance(obj, (list, tuple, dict, set))


def isiterable(obj):
    return isinstance(obj, basestring) or issequence(obj)


def all_true(obj):
    """
    Return a list with all the non None, empty or False objects contained
    in the passed object.
    """
    return [e for e in obj if e]
    

def make_list(obj):
    """Make a directly iterable list from an object or a recursive list."""
    if not issequence(obj):
        obj_valid = [obj]
    else:
        # if obj is just one element into subsequences, then we just unpack it
        obj_valid = list(obj)[:]
        while isinstance(obj_valid, list) and len(obj_valid) == 1 and isinstance(obj_valid[0], list):
            obj_valid = obj_valid[0]

    return obj_valid

