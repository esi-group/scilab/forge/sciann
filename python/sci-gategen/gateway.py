"""
Gateway module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/


# Module methods
# Tests on the parameter, the test against None is important
def is_source(gw_parameter):
    return True if gw_parameter.position_source != None else False

def is_source_return(gw_parameter):
    return True if gw_parameter.position_return == -1 else False

def is_destination(gw_parameter):
    return True if gw_parameter.position_destination != None else False

def is_return(gw_parameter):
    return True if gw_parameter.position_return != None else False


class GatewayParameter:
    """
    Stores the necessary information regarding a gateway parameter.

    :IVariables:
        self.name_type : string
            Type for the parameter. 
        self.name_parameter : string
            Name of the parameter.
        self.position_source : integer
            Position of the parameter in the source prototype.
            (ie: in the source language function)
        self.position_destination : integer
            Position of the parameter in the destination prototype.
            (ie: in the gateway function interface)
        self.position_return : integer
            Position of the parameter in the destination return list.
            (ie: in the gateway function return list)
    """

    POSITION_END = 0

    def __init__(self, name_type=None, name_parameter=None, position_source=None, position_destination=None, position_return=None, nb_pointers=None):
        """
        Initializer.

        :Parameters:
            name_type : string
                Type for the parameter. 
            name_parameter : string
                Name of the parameter.
            position_source : integer
                Position of the parameter in the source prototype.
                (ie: in the source language function)
            position_destination : integer
                Position of the parameter in the destination prototype.
                (ie: in the gateway function interface)
            position_return : integer
                Position of the parameter in the destination return list.
                (ie: in the gateway function return list)
        """
        self.name_type = name_type
        self.name_type_original = name_type
        self.name_parameter = name_parameter
        self.name_parameter_lower = name_parameter
        self.name_parameter_new = name_parameter
        self.position_source = position_source
        self.position_destination = position_destination
        self.position_return = position_return
        self.nb_pointers = nb_pointers

        # For the position_* attributes, the convention for the values is
        # as follow:
        #   * None: The parameter has no position in the list.
        #   * 0: The parameter is present in the list. The position of the
        #     parameter is not required at this step, and will be determined
        #     later.
        #   * >0: The parameter is present in the list. The position of the
        #     parameter is required.
        #   * <0: Only for source position, means that the parameter is in
        #     the source function prototype as a return parameter


    def to_parameter_matrix(self):
        return GatewayParameterMatrix(self.name_type, self.name_parameter, self.position_source, self.position_destination, self.position_return, self.nb_pointers)


    def to_parameter_dimension(self):
        return GatewayParameterDimension(self.name_type, self.name_parameter, self.position_source, self.position_destination, self.position_return, self.nb_pointers)



class GatewayParameterDimension(GatewayParameter):
    """
    Stores the necessary information regarding a parameter that is also
    the dimension of a matrix.
    """

    def __init__(self, name_type, name_parameter, position_source, position_destination, position_return, nb_pointers):
        GatewayParameter.__init__(self, name_type, name_parameter, position_source, position_destination, position_return, nb_pointers)
        self.parameter_matrix = None



class GatewayParameterMatrix(GatewayParameter):
    """
    Stores the necessary information regarding a matrix gateway parameter.
    """

    def __init__(self, name_type, name_parameter, position_source, position_destination, position_return, nb_pointers):
        GatewayParameter.__init__(self, name_type, name_parameter, position_source, position_destination, position_return, nb_pointers)
        self.parameter_dimension_row = None 
        self.parameter_dimension_column = None 
        self.parameter_memory_allocation_needed = False



class GatewayPrototype:
    """
    Stores prototypes for the gateway functions.

    :IVariables:
        self.name_function : string
            Name of the function of which the prototype is being handled,
            as it appears in the source language.
        self.name_function_new : string
            Name of the function that will be used in the gateway.
        self.name_class : string
            Name of the class to which the pre-cited function belongs to.
            Equals to None if the language does not support classes.
        self.parameters : sequence of `GatewayParameter`
            Store the parameters.
        self.nb_parameters_source : integer
            Store the number of source parameters.
        self.nb_parameters_destination : integer
            Store the number of destination parameters.
        self.nb_parameters_return : integer
            Store the number of return parameters.
        self.hooks_start : sequence
            Sequence of lines that have to be added at the beginning of
            the gateway.
        self.hooks_end : sequence
            Sequence of lines that have to be added at the end of
            the gateway.
    """

    def __init__(self, name_class=None, name_function=None):
        """
        Initializer.
        """
        self.name_class = name_class
        self.name_function = name_function
        self.name_function_lower = name_function
        self.name_function_new = name_function
        self.name_gateway = name_function
        self.parameters = []
        self.comments = ''
        self.nb_parameters_source = 0
        self.nb_parameters_destination = 0
        self.nb_parameters_return = 0
        self.hooks_start = []
        self.hooks_end = []
        self.error_type = False


    def add_parameter_source(self, parameter):
        if parameter != None:
            self.parameters.append(parameter)
            self.nb_parameters_source += 1


    def get_parameters_source(self):
        return [p for p in self.parameters if p.position_source != None and p.position_source != -1]


    def add_parameter_destination(self, parameter, position_destination=GatewayParameter.POSITION_END):
        parameter.position_destination = position_destination
        self.parameters.append(parameter)
        # put the added parameter at the head of the list
        self.parameters = self.parameters[-1:] + self.parameters[:-1]
        self.nb_parameters_destination += 1


    def get_parameters_destination(self):
        return [p for p in self.parameters if p.position_destination != None]


    def add_parameter_return(self, parameter, position_return=GatewayParameter.POSITION_END, position_source=None):
        parameter.position_return = position_return
        parameter.position_source = position_source
        self.parameters.append(parameter)
        self.nb_parameters_return += 1


    def get_parameters_return(self):
        return [p for p in self.parameters if p.position_return != None]

    # TODO rename this method?
    def get_parameter_return_source(self):
        for parameter in self.parameters:
            if parameter.position_source == -1:
                return parameter
        return None


    def is_parameter_return_source(self, gw_parameter):
        if gw_parameter and gw_parameter == self.get_parameter_return_source():
            return True
        else:
            return False


    def get_parameters_source_and_destination(self):
        return [p for p in self.parameters if p.position_source != None or p.position_destination != None]


    def get_parameter_by_name(self, name_parameter):
        for parameter in self.parameters:
            if name_parameter == parameter.name_parameter:
                return parameter
        return None


    def get_parameters(self):
        return self.parameters



    #def delete_parameter(self, parameter_delete):
    #    for index, parameter_current in enumerate(self.parameters):
    #        if parameter_current == parameter_delete:
    #            if parameter_current.get_position_source != None:
    #                self.nb_parameters_source -= 1
    #            elif parameter_current.get_position_destination != None:
    #                self.nb_parameters_destination -= 1
    #            else
    #                self.nb_parameters_return -= 1
    #            del self.parameters[index]


    
    def overwrite_parameter(self, parameter_old, parameter_new):
        for index, parameter_current in enumerate(self.parameters):
            if parameter_current == parameter_old:
                self.parameters[index] = parameter_new 
                break


    def add_hook_start(self, line):
        self.hooks_start.append(line)


    def add_hook_end(self, line):
        self.hooks_end.append(line)


    def get_strings_parameters(self):
        parameters = [parameter.name_type_original for parameter in self.parameters]
        parameters.extend([parameter.name_parameter for parameter in self.parameters])
        return parameters



class GatewayFile:
    """
    Associates `GatewayPrototype` instances to a file.

    :IVariables:
        name_file : string
            Name of the file where prototypes have to be written.
        gw_prototypes : GatewayPrototype
            Prototypes to be written. 
    """
    
    def __init__(self, name_file):
        self.name_file = name_file
        self.gw_prototypes = []
        self.headers = []

