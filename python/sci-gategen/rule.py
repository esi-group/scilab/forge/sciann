"""
Rule module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/


import re


# Module variables
rules_model = {}


# Module methods
def add_rule_model(rule):
    # TODO strip text before the '.', and not 'rule.'
    type = str(rule.__class__).lstrip('rule.')
    rules_model[type] = rule


def check_regex(string_regex, string_input, type_search='search'):
    if string_regex != None:
        regex = re.compile(string_regex)#, re.VERBOSE)
        function_search = getattr(regex, type_search)
        #match = regex.match(string_input)
        #print 'input:', string_input
        match = function_search(string_input)
        if match != None:
            return True
        else:
            return False
    else:
        return True


def check_regex_seq(string_regex, strings_input, type_search='search'):
    return any([check_regex(string_regex, string_input, type_search) for string_input in strings_input]) \
        or not strings_input


def build_rule(string):
    for rule_model in rules_model.values():
        match = rule_model.regex.match(string)
        if match != None:
            return rule_model.build_rule(match)
        #rule = rule_model.build_rule(string)
        #if rule != None:
        #    break

    return None



class Rule:
    format = r"""[^.]*"""
    # Unmatchable expression, so that it is impossible
    # to use the abtract class Rule.

    def __init__(self):
        pass



class RuleDirectoryPermission(Rule):
    format = r"""
             [ ]*?(?P<action>input|output)
             [ ]+directory
             [ ]+(?P<name_directory>[^\s]+)
             [\s]*?
             ([#].*?)?
             $
             """
    regex = re.compile(format, re.VERBOSE)

    def __init__(self, action=None, name_directory=None):
        Rule.__init__(self)
        self.action = action
        self.name_directory = name_directory


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('action'), match.group('name_directory'))



class RuleFilePermission(Rule):
    format = r"""
             [ ]*?(?P<action>include|exclude)
             [ ]+file
             [ ]+(?P<name_file>[^\s]+)
             [\s]*?
             ([#].*?)?
             $
             """
    regex = re.compile(format, re.VERBOSE)

    def __init__(self, action=None, name_file=None):
        Rule.__init__(self)
        self.action = action
        self.name_file = name_file


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('action'), match.group('name_file'))



class RuleFunctionPermission(Rule):
    format = r"""
             [ ]*?(?P<action>include|exclude)
             [ ]+function
             [ ]+(?P<name_function>[^\s]+)
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+))?
             [\s]*?
             ([#].*?)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, action=None, name_function=None, name_file=None):
        Rule.__init__(self)
        self.action = action
        self.name_function = name_function
        self.name_file = name_file


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('action'), match.group('name_function'), match.group('name_file'))



class RuleParameterPermission(Rule):
    format = r"""
             [ ]*?(?P<action>include|exclude)
             [ ]+parameter
             [ ]+(?P<name_parameter>[^\s]+)
             ([ ]+in[ ]function[ ]+(?P<name_function>[^\s]+))?
             [ ]*?
             ([#].*?)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, action=None, name_parameter=None, name_function=None, name_file=None):
        Rule.__init__(self)
        self.action = action
        self.name_parameter = name_parameter
        self.name_function = name_function
        self.name_file = name_file

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('action'), match.group('name_parameter'), match.group('name_function'))



class RuleParameterAdd(Rule):
    format = r"""
             [ ]*add
             [ ]+parameter
             [ ]+(?P<name_parameter>.+?)
             ([ ]+in[ ]function[ ]+(?P<name_function>[^\s]+))
             ([ ]+with[ ]parameter[ ]+(?P<name_parameter_present>[^\s]+))?
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+))?
             [ ]*
             ([#].*)?
             $
             """
    regex = re.compile(format, re.VERBOSE)

    def __init__(self, name_parameter=None, name_function=None, name_parameter_present=None, name_file=None):
        Rule.__init__(self)
        self.name_parameter = name_parameter
        self.name_function = name_function
        self.name_parameter_present = name_parameter_present
        self.name_file = name_file

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('name_parameter'), match.group('name_function'), match.group('name_parameter_present'), match.group('name_file'))



class RuleParameterReturn(Rule):
    format = r"""
             [ ]*return
             [ ]+parameter
             [ ]+(?P<name_parameter>[^\s]+)
             ([ ]+in[ ]function[ ]+(?P<name_function>[^\s]+))?
             ([ ]+with[ ]parameter[ ]+(?P<name_parameter_present>[^\s]+))?
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+))?
             [ ]*
             ([#].*)?
             $
             """
    regex = re.compile(format, re.VERBOSE)

    def __init__(self, name_parameter=None, name_function=None, name_parameter_present=None, name_file=None):
        Rule.__init__(self)
        self.name_parameter = name_parameter
        self.name_function = name_function
        self.name_parameter_present = name_parameter_present
        self.name_file = name_file

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('name_parameter'), match.group('name_function'), match.group('name_parameter_present'), match.group('name_file'))



class RuleParameterMatrix(Rule):
    format = r"""
             [ ]*matrix
             [ ]+parameter
             [ ]+(?P<name_parameter>[^\s]+)
             ([ ]+col:(?P<name_dim_col>[^\s]+))
             ([ ]+row:(?P<name_dim_row>[^\s]+))?
             ([ ]+in[ ]function[ ]+(?P<name_function>[^\s]+))?
             ([ ]+with[ ]parameter[ ]+(?P<name_parameter_present>[^\s]+))?
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+))?
             [ ]*
             ([#].*)?
             $
             """
    regex = re.compile(format, re.VERBOSE)

    def __init__(self, name_parameter=None, name_dim_row=None, name_dim_col=None, name_function=None, name_parameter_present=None, name_file=None):
        Rule.__init__(self)
        self.name_parameter = name_parameter
        self.name_dim_row = name_dim_row
        self.name_dim_col = name_dim_col
        self.name_function = name_function
        self.name_parameter_present = name_parameter_present
        self.name_file = name_file

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('name_parameter'), match.group('name_dim_row'), match.group('name_dim_col'), match.group('name_function'), match.group('name_parameter_present'), match.group('name_file'))



class RuleHook(Rule):
    format = r"""
             [ ]*hook
             [ ]*?(?P<location>start|end)
             [ ]+(?P<line>.+?)
             ([ ]+in[ ]function[ ]+(?P<name_function>[^\s]+?))
             ([ ]+with[ ]parameter[ ]+(?P<name_parameter_present>[^\s]+))?
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+?))?
             [\s]*
             ([#].*)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, location=None, line=None, name_function=None, name_parameter_present=None, name_file=None):
        Rule.__init__(self)
        self.location = location
        self.line = line
        self.name_function = name_function
        self.name_parameter_present = name_parameter_present
        self.name_file = name_file

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('location'), match.group('line'), match.group('name_function'), match.group('name_parameter_present'), match.group('name_file'))



class RuleAlias(Rule):
    format = r"""
             [ ]*alias
             [ ]+(?P<source>.+?)
             [ ]+into
             [ ]+(?P<destination>.+?)
             [\s]*
             ([#].*)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, source=None, destination=None):
        Rule.__init__(self)
        self.source = source
        self.destination = destination


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('source'), match.group('destination'))

    # REMEMBER THIS ONE IS DIFFERENT
    #def check_source(self, string):
    #    return check_regex(self.source, string, 'search')



class RuleReplacement(Rule):
    format = r"""
             [ ]*replace
             [ ]*?(?P<kind>parameter|function)
             [ ]+(?P<source>.+?)
             [ ]+into
             [ ]+(?P<destination>.+?)
             [\s]*
             ([#].*)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, source=None, destination=None, kind=None):
        Rule.__init__(self)
        self.source = source
        self.destination = destination
        self.kind = kind


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('source'), match.group('destination'), match.group('kind'))



class RuleFile(Rule):
    format = r"""
             [ ]*output
             [ ]+file
             [ ]+(?P<name_file>[^\s]+)
             [ ]+(?P<name_function>.+?)
             ([ ]+with[ ]parameter[ ]+(?P<name_parameter_present>[^\s]+))?
             [\s]*?
             ([#].*?)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, name_file=None, name_function=None, name_parameter_present=None):
        Rule.__init__(self)
        self.name_file = name_file
        self.name_function = name_function
        self.name_parameter_present = name_parameter_present


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('name_file'), match.group('name_function'), match.group('name_parameter_present'))



class RuleLowerCase(Rule):
    format = r"""
             [ ]*lower[ ]case
             [ ]+(?P<kind>parameters|functions)
             [\s]*?
             ([#].*?)?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, kind=None):
        Rule.__init__(self)
        self.kind = kind


    @classmethod
    def build_rule(cls, match):
        return cls(match.group('kind'))


class RuleHeader(Rule):
    format = r"""
             [ ]*?header
             [ ]+(?P<header>.+?)
             ([ ]+in[ ]file[ ]+(?P<name_file>[^\s]+))?
             [ ]*?
             $
             """

    regex = re.compile(format, re.VERBOSE)

    def __init__(self, name_file=None, header=None):
        Rule.__init__(self)
        self.name_file = name_file
        self.header = header


    @classmethod
    def build_rule(cls, match):
        #return cls('', match.group('header'))
        return cls(match.group('name_file'), match.group('header'))



# Metaclass for all field classes
class RuleField(Rule):
    format = r"""
             [ ]*%s 
             [ ]+(?P<value>.*?)
             [\s]*?
             ([#].*?)?
             $
             """

    def __init__(self, value=None):
        Rule.__init__(self)
        self.value = value
        #print str(self.__class__).lstrip('rule.'), self.name_field
        self.regex = re.compile(self.format % (self.name_field), re.VERBOSE)

    @classmethod
    def build_rule(cls, match):
        return cls(match.group('value'))


# Metainstances of RuleField
RuleFieldAuthor = type(RuleField)('RuleFieldAuthor', (RuleField,), {'name_field': 'author'})
RuleFieldProject = type(RuleField)('RuleFieldProject', (RuleField,), {'name_field': 'project'})
RuleFieldLibrary = type(RuleField)('RuleFieldLibrary', (RuleField,), {'name_field': 'compiler[ ]library'})
RuleFieldLink = type(RuleField)('RuleFieldLink', (RuleField,), {'name_field': 'compiler[ ]link'})
RuleFieldInclude = type(RuleField)('RuleFieldInclude', (RuleField,), {'name_field': 'compiler[ ]include'})
RuleFieldLanguage = type(RuleField)('RuleFieldLanguage', (RuleField,), {'name_field': 'language'})



class RuleSet:

    def __init__(self):
        self.rules = {}
        

    def add_rule(self, rule):
        type = str(rule.__class__).lstrip('rule.')
        if type not in self.rules:
           self.rules[type] = [] 

        self.rules[type].append(rule)


    def get_rules_all(self):
        for rules in self.rules.values():
            for rule_current in rules:
                yield rule_current


    def get_rules_by_type(self, type):
        if type not in self.rules:
            return ()
        else: 
            return tuple(self.rules[type])


    def __run_checks_obsolete(self, rule, checks):
        check_rule = True
        for name_function, string_input in checks.items():
            function = getattr(rule, name_function)
            if function(string_input) == False:
                check_rule = False
                break
        return check_rule 


    def __run_checks(self, rule, checks, type_search):
        # TODO modify using list comprehension and all()
        for name_parameter, strings_input in checks.items():
            value_parameter = getattr(rule, name_parameter)
            if not isinstance(strings_input, list):
                strings_input = [strings_input]

            if all([not check_regex(value_parameter, string_input, type_search) for string_input in strings_input]):
                return False

        return True


    def check_permission(self, type_rule, checks, type_search='search'):
        """
        Check the permission, by listing the include and exclude rules.
        """
        action = {'include': False, 'exclude': False}
        for rule in self.get_rules_by_type(type_rule):
            if self.__run_checks(rule, checks, type_search):
                action[rule.action] = True

        # Is equivalent to say that first, everything is included,
        # then the 'exclude' rules are checked, and finally the
        # 'include' rules are checked.
        if action['exclude'] and not action['include']:
            return False
        else:
            return True


    def check_field(self, type_rule, checks, type_search='search'):
        """
        Check the given values against the regex stored in the rules.
        As soon as a rule matches for only one of the value, True is returned.
        """
        for rule in self.get_rules_by_type(type_rule):
            if self.__run_checks(rule, checks, type_search):
                return True, rule
        return False, None


    def get_parameters_added(self, name_function, strings_parameters, name_file):
        for rule in self.get_rules_by_type('RuleParameterAdd'):
            #TODO add the test on name_class
            #if rule.check_name_function(name_function) == True:
            if check_regex(rule.name_function, name_function) and \
              check_regex_seq(rule.name_parameter_present, strings_parameters):
                    yield rule.name_parameter


    def get_rule_parameters_matrix(self, name_parameter, name_function, strings_parameters, name_file):
        for rule in self.get_rules_by_type('RuleParameterMatrix'):
            #TODO add the test on name_function
            #TODO add the test on name_class
            #if rule.check_name_parameter(name_parameter) == True:
            if check_regex(rule.name_parameter, name_parameter) and \
              check_regex_seq(rule.name_parameter_present, strings_parameters):
                return True, rule
        return False, None
        

    def get_hooks(self, name_function, strings_parameters, name_file):
        for rule_hook in self.get_rules_by_type('RuleHook'):
            #TODO add the test on name_class
            #if rule.check_name_function(name_function) == True:
            if check_regex(rule_hook.name_function, name_function) and \
              check_regex_seq(rule_hook.name_parameter_present, strings_parameters):
                yield (rule_hook.location, rule_hook.line)

    def get_headers(self, name_file):
        for rule_header in self.get_rules_by_type('RuleHeader'):
            if check_regex(rule_header.name_file, name_file):
                yield rule_header.header


    def get_replacements(self, name_function, kind):
        for rule_replace in self.get_rules_by_type('RuleReplacement'):
            if kind == rule_replace.kind and check_regex(rule_replace.source, name_function):
                yield (rule_replace.source, rule_replace.destination)
            # REMEMBER the check has to be done with 'search' and not 'match'


    def get_alias(self, name_type):
        for rule_alias in self.get_rules_by_type('RuleAlias'):
            if check_regex(rule_alias.source, name_type) == True:
                return rule_alias.destination
            # REMEMBER the check has to be done with 'search' and not 'match'

            # old system: just replace the string, but not a good conception
            #regex = re.compile(rule.source) 
            #replacement = regex.subn(rule.destination, name_type)
            #if replacement[1] > 0:
            #    return replacement[0]

        return None
        

    def get_rule_file(self, name_function):
        for rule in self.get_rules_by_type('RuleFile'):
            if check_regex(rule.name_function, name_function) == True:
                return True, rule
        return False, None


    def is_lower_case_activated(self, kind):
        for rule_lc in self.get_rules_by_type('RuleLowerCase'):
            if rule_lc.kind == kind:
                return True
        return False


    def print_dict(self):
        print self.rules



# Add an instance of each kind of rule to the Rule base class, so that
# each kind of rule can be tested directly from there.
add_rule_model(RuleDirectoryPermission())
add_rule_model(RuleFilePermission())
add_rule_model(RuleFunctionPermission())
add_rule_model(RuleParameterPermission())
add_rule_model(RuleParameterAdd())
add_rule_model(RuleParameterReturn())
add_rule_model(RuleParameterMatrix())
add_rule_model(RuleHook())
add_rule_model(RuleAlias())
add_rule_model(RuleReplacement())
add_rule_model(RuleFile())
add_rule_model(RuleLowerCase())
add_rule_model(RuleHeader())
add_rule_model(RuleFieldAuthor())
add_rule_model(RuleFieldProject())
add_rule_model(RuleFieldLibrary())
add_rule_model(RuleFieldLink())
add_rule_model(RuleFieldInclude())
add_rule_model(RuleFieldLanguage())
