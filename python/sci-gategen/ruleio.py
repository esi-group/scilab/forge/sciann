"""
Rules I/O module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/



import sys
import re
import logging

import rule
#from rule import Rule


logger = logging.getLogger('ruleio')


class RuleIO_File:
    """
    Rule file input/output class 
    
    :IVariables:
        __stream : Stream 
            Stream instance used for the I/O operations. In the case of this
            file module, the stream is the prefix of the file to be used.
    """

    def __init__(self, stream=None):
        """
        Initializer
        
        :Parameters:
            stream : string 
                Prefix of the file to be used.
        """
        self.stream = stream



    def read(self, rule_set):
        """
        Read a file and fill the provided `RuleSet`.
	    """
        regex_comment = re.compile(r"""
                                   [ ]*
                                   ([#].*)?
                                   $
                                   """, re.VERBOSE)

        try:
            file = open(self.stream, "rb")
            for index_line, line in enumerate(file):

                # skip blank and comment lines
                if regex_comment.match(line):
                    continue

                is_valid_rule = False
                message = None
                rule_current = rule.build_rule(line)
                if rule_current:
                    is_valid_rule = True
                    # TODO test here for invalid regular expressions

                    #print 'Valid:', index_line + 1
                    #try:
                    #    regex = re.compile(rule_current.format)
                    #except sre_constants.error, e:
                    #    message = 'Invalid regular expression on line:' + str(index_line + 1)
                else:
                    message = 'Invalid rule on line %(line)d' % {'line': index_line + 1}

                if is_valid_rule: 
                    rule_set.add_rule(rule_current)

                if message:
                    logger.warning(message)

        except IOError, e:
            #string_error = 'Unable to read the file: ' + filename
            #yaise SggStreamError, string_error
            raise IOError, e
        finally:
            file.close()
